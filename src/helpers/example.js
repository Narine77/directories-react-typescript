import React, {Fragment, useEffect, useMemo, useState} from 'react';
import {useTypeSelector} from '../hooks/userTypeSelector';
import {useActions} from "../hooks/useAction";
import {getProjectsRequest} from "../store/actions/project";
import {Link} from 'react-router-dom';
import Button from '../components/form/Button';
import {useDispatch} from 'react-redux';
import {deleteRoomRequest, getProjectRoomRequest} from "../store/actions/room";
import Room from "./Room";
import {getProjectFolderRequest, getRoomFolderRequest} from "../store/actions/folder";

export const Home: React.FC = () => {
    const [openProjectId, setOpenProjectId] = useState(null);
    const [openRoomId, setOpenRoomId] = useState(null);
    const {rooms, loading, error} = useTypeSelector(state => state.rooms)
    const {folders} = useTypeSelector(state => state.folders)
    const {projects} = useTypeSelector(state => state.projects)
    const {getRoomsRequest} = useActions()
    const {getProjectsRequest} = useActions()
    const {getProjectRoomRequest} = useActions()
    const {getProjectFolderRequest} = useActions()
    const {getRoomFolderRequest} = useActions()
    const {deleteRoomRequest} = useActions()
    const {deleteProjectRequest} = useActions()

    useMemo(() => {
        getRoomsRequest()
        console.log(rooms, 'rooms')
        getProjectsRequest()
        console.log(projects, 'projects')

    }, [])
    useEffect(() => {
        if (openProjectId) {
            getProjectRoomRequest(openProjectId)
            getProjectFolderRequest(openProjectId)
            console.log(openProjectId, 'openProjectId')
        }
    }, [openProjectId])

    useEffect(() => {
        if (openRoomId) {
            getRoomFolderRequest(openRoomId)
            console.log(openRoomId, 'openRoomId')
        }
    }, [openRoomId])

    const handleDelete = async (id: any) => {
        console.log(id)
        await deleteProjectRequest(id);
        getProjectsRequest()
    };

    const toggleProject = (id: any) => {
        if (id === openProjectId) {
            console.log(id, 'idd')
            setOpenProjectId(null)
        } else {
            setOpenProjectId(id)

            console.log('gggg')
        }
    }
    const toggleRoom = (id: any) => {
        if (id === openRoomId) {
            setOpenRoomId(null)
        } else {
            setOpenRoomId(id)
            console.log(id, 'roooom')
        }
    }
    return (
        <div>
            <div className="home">
                <div className="select-block">
                </div>
                <div className="product-container">
                    <Link to="/create-project">
                        <button className="btn-green">Create Room</button>
                    </Link>
                    {projects?.map((item: any) => (
                        <Fragment key={item.id}>
                            <div className="product">
                                {item.avatar ? (
                                    <img onClick={() => {
                                        toggleProject(item.id);
                                    }} className="project-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}
                                         alt=""/>) : null}

                                <div className="product-name">{item.name}</div>
                                <div className="btn-block">
                                    <Link key={item.id} to={`/project/update/${item.id}`}>
                                        <button className="btn">Update</button>
                                    </Link>
                                    <button className="btn" type="button" id={item.id}
                                            onClick={() => handleDelete(item.id)}>Delete
                                    </button>
                                </div>
                            </div>
                            <div className={openProjectId === item.id ? 'open-category' : 'close-category'}>
                                {openProjectId === item.id ? (<div className="room-container">
                                    {
                                        rooms?.map((item: any) => (
                                            <div  key={item.id}>
                                                <div className="room-container-element">
                                                    <img className="room-img"
                                                         onClick={() => {
                                                             toggleRoom(item.id) }}
                                                         src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}
                                                         alt=""/>
                                                    <div>{item.name}</div>
                                                    <div className="btn-block">
                                                        <Link key={item.id} to={`/project/update/${item.id}`}>
                                                            <button className="btn">Update</button>
                                                        </Link>
                                                        <button className="btn" type="button" id={item.id}
                                                                onClick={() => handleDelete(item.id)}>Delete
                                                        </button>
                                                    </div>
                                                </div>
                                                <div className={openRoomId === item.id ? 'open-category' : 'close-category'}>
                                                    {openRoomId === item.id ? (<div>
                                                        {folders.map((item:any)=>(
                                                            <div key={item.id}>{item.name}{item.id}</div>
                                                        ))}
                                                    </div>) :  null}
                                                </div>
                                            </div>
                                        ))}
                                    {
                                        folders?.map((item: any) => (<div className="room-container-element" key={item.id}>
                                                <img className="folder-img"
                                                     src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}
                                                     alt=""/>
                                                <div>{item.name}</div>
                                                <div className="btn-block">
                                                    <Link key={item.id} to={`/project/update/${item.id}`}>
                                                        <button className="btn">Update</button>
                                                    </Link>
                                                    <button className="btn" type="button" id={item.id}
                                                            onClick={() => handleDelete(item.id)}>Delete
                                                    </button>
                                                </div>
                                            </div>
                                        ))}
                                </div>) : null}
                            </div>
                        </Fragment>
                    ))}
                    {/*{rooms?.map((item: any) => (*/}
                    {/*    <div className="product" key={item.id}>*/}
                    {/*        {item.avatar ? (*/}
                    {/*            <img className="room-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}*/}
                    {/*                 alt=""/>) : null}*/}
                    {/*        <div className="product-name">{item.name}</div>*/}
                    {/*        /!*<h4>{item.projects?.map((i:any)=>(*!/*/}
                    {/*        /!*    <h5 key={i.id}>{i.id}</h5>*!/*/}
                    {/*        /!*))}</h4>*!/*/}
                    {/*        <div className="btn-block">*/}
                    {/*            <Link key={item.id} to={`/room/update/${item.id}`}><button className="btn">Update</button></Link>*/}
                    {/*            <button className="btn" type="button" id={item.id} onClick={()=> handleDelete(item.id)}>Delete</button>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*))}*/}
                </div>

            </div>
        </div>
    );
}
export default Home;


