import axios from "axios";
import { serialize } from "object-to-formdata";

const api = axios.create({
    baseURL: 'http://localhost:6500',
    headers: {
        'content-type' : 'application/json',
    }
});
const api2 = axios.create({
    baseURL: 'http://localhost:6500',
});
const toFormData = (data:{[key: string]: string}, indices = false) => serialize({ ...data }, { indices });
class Api{
    // create
    static createRoom(formData:{[key: string]: string}){
        return api2.post('room/create', toFormData(formData) )
    }
    // static createRoom(data:{[key: string]: string}){
    //     const formData:FormData = new FormData();
    //     for (const key in data){
    //         formData.append(key, data[key]);
    //     }
    //     return api2.post('room/create', formData)
    // }
    static createProject(formData:{[key: string]: string}){
        return api2.post('project/create', toFormData(formData) )
    }
    // get
    static  getAllRooms(){
        return api.get('room/getAll')
    }
    static  getAllProjects(){
        return api.get('project/getAll')
    }
    static  getAllFolders(){
        return api.get('folder/getAll')
    }
    static  getAllDocuments(){
        return api.get('document/getAll')
    }

    static  getProjectRooms(id:number){
        return api.get(`room/project/${id}`)
    }
    static  getProjectFolders(id:number){
        return api.get(`folder/project/${id}`)
    }
    static  getRoomFolders(id:number){
        return api.get(`folder/room/${id}`)
    }
    static  getRoomDocuments(id:number){
        return api.get(`document/room/${id}`)
    }
    static  getFolderDocuments(id:number){
        return api.get(`document/folder/${id}`)
    }
    static getSingleProject(id: number){
        return api.get(`project/getOne/${id}`)
    }
    static getSingleRoom(id: number){
        return api.get(`room/getOne/${id}`)
    }
    static getSingleFolder(id: number){
        return api.get(`folder/getOne/${id}`)
    }


    // edit
    static editProject(id: number, formData:{[key: string]: string}){
        return api2.put(`project/edit/${id}`, toFormData(formData))
    }
    static editRoom(id: number, formData:{[key: string]: string}){
        return api2.put(`room/edit/${id}`,  toFormData(formData))
    }
    static  editFolderRoomId(id:number){
        return api.put(`folder/room/edit/${id}`)
    }  static  deleteFolderProjectId(id:number){
        return api.post(`folder/project/delete/${id}`)
    }

    // static  deleteRoom(id:number){
    //     return api.get('room/delete/', { params: { id: id } })
    // }

    static  deleteRoom(id:number){
        return api.delete(`room/delete/${id}`)
    }
    static  deleteProject(id:number){
        return api.delete(`project/delete/${id}`)
    }
}

export default Api
