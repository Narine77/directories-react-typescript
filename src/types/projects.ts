import {Room, RoomActionTypes, roomCreateTypes} from "./room";

export interface Project {
    name: string,
    description: string,
    avatar: string
}
export interface ProjectState {
    projects: Project[],
    loading: boolean,
    error: null | string,
    project: Project
}
export interface projectObjectTypes{
    [key:string]:string
}
export interface projectEditTypes<P>{
    project: P,
}
export interface projectCreateTypes<T>{
    project: T,
}
export enum ProjectActionTypes{
    GET_PROJECTS_REQUEST = "GET_PROJECTS_REQUEST",
    GET_PROJECTS_SUCCESS = "GET_PROJECTS_SUCCESS",
    GET_PROJECTS_FAIL = "GET_PROJECTS_FAIL",

    GET_All_PROJECTS_REQUEST = "GET_All_PROJECTS_REQUEST",
    GET_All_PROJECTS_SUCCESS = "GET_All_PROJECTS_SUCCESS",
    GET_ALL_PROJECTS_FAIL = "GET_ALL_PROJECTS_FAIL",

    GET_SINGLE_PROJECT_REQUEST = "GET_SINGLE_PROJECT_REQUEST",
    GET_SINGLE_PROJECT_SUCCESS = "GET_SINGLE_PROJECT_SUCCESS",
    GET_SINGLE_PROJECT_FAIL = "GET_SINGLE_PROJECT_FAIL",

    DELETE_PROJECTS_REQUEST = "DELETE_PROJECTS_REQUEST",
    DELETE_PROJECTS_SUCCESS = "DELETE_PROJECTS_SUCCESS",
    DELETE_PROJECTS_FAIL = "DELETE_PROJECTS_FAIL",

    CREATE_PROJECT_REQUEST = "CREATE_PROJECT_REQUEST",
    CREATE_PROJECT_SUCCESS = "CREATE_PROJECT_SUCCESS",
    CREATE_PROJECT_FAIL = "CREATE_PROJECT_FAIL",

    EDIT_PROJECT_REQUEST = "EDIT_PROJECT_REQUEST",
    EDIT_PROJECT_SUCCESS = "EDIT_PROJECT_SUCCESS",
    EDIT_PROJECT_FAIL = "EDIT_PROJECT_FAIL",

}
// create
interface CreateProjectRequestAction{
    type: ProjectActionTypes.CREATE_PROJECT_REQUEST,
    payload? : any
}

interface CreateProjectSuccessAction{
    type: ProjectActionTypes.CREATE_PROJECT_SUCCESS,
    payload: projectCreateTypes<object>
}

interface CreateProjectFailAction{
    type: ProjectActionTypes.CREATE_PROJECT_FAIL,
    payload? : any
}

// get
interface GetProjectRequestAction{
    type: ProjectActionTypes.GET_PROJECTS_REQUEST,
    payload? : any
}

interface GetProjectSuccessAction{
    type: ProjectActionTypes.GET_PROJECTS_SUCCESS,
    payload: []
}

interface GetProjectFailAction{
    type: ProjectActionTypes.GET_PROJECTS_FAIL,
    payload? : any
}
//get one
interface GetSingleProjectRequestAction{
    type: ProjectActionTypes.GET_SINGLE_PROJECT_REQUEST,
    payload? : any
}

interface GetSingleProjectSuccessAction{
    type: ProjectActionTypes.GET_SINGLE_PROJECT_SUCCESS,
    payload: []
}

interface GetSingleProjectFailAction{
    type: ProjectActionTypes.GET_SINGLE_PROJECT_FAIL,
    payload? : any
}

// get all
interface GetProjectsRequestAction{
    type: ProjectActionTypes.GET_All_PROJECTS_REQUEST,
    payload? : any
}

interface GetAllProjectSuccessAction{
    type: ProjectActionTypes.GET_All_PROJECTS_SUCCESS,
    payload: []
}

interface GetAllProjectFailAction{
    type: ProjectActionTypes.GET_ALL_PROJECTS_FAIL,
    payload? : any
}
// edit
interface EditProjectRequestAction{
    type: ProjectActionTypes.EDIT_PROJECT_REQUEST,
    payload? : any
}

interface EditProjectSuccessAction{
    type: ProjectActionTypes.EDIT_PROJECT_SUCCESS,
    payload: projectEditTypes<projectObjectTypes>
}

interface EditProjectFailAction{
    type: ProjectActionTypes.EDIT_PROJECT_FAIL,
    payload? : any
}
// delete

interface DeleteProjectRequestAction{
    type: ProjectActionTypes.DELETE_PROJECTS_REQUEST,
    payload? : any
}

interface DeleteProjectSuccessAction{
    type: ProjectActionTypes.DELETE_PROJECTS_SUCCESS,
    payload: []
}

interface DeleteProjectFailAction{
    type: ProjectActionTypes.DELETE_PROJECTS_FAIL,
    payload? : any
}
export type ProjectAction = CreateProjectRequestAction | CreateProjectSuccessAction | CreateProjectFailAction
    | GetProjectRequestAction | GetProjectSuccessAction | GetProjectFailAction
    | GetSingleProjectRequestAction | GetSingleProjectSuccessAction | GetSingleProjectFailAction
    | DeleteProjectRequestAction | DeleteProjectSuccessAction | DeleteProjectFailAction |
    GetProjectsRequestAction | GetAllProjectSuccessAction | GetAllProjectFailAction
    | EditProjectRequestAction | EditProjectSuccessAction | EditProjectFailAction
