import React from 'react';
import Loader from '../Loader';

function Button(props:any) {
  const {
    className, type, icon, title, loading, disabled, ...restProps
  } = props;
  return (
    <button className={`btn btn-teal ${className}`} type={type} disabled={loading || disabled} {...restProps}>
      {icon || null}
      {loading ? <Loader /> : title}
    </button>
  );
}

export default Button;
