import {Dispatch} from "redux";
import Api from "../../Api";
import {ProjectAction, ProjectActionTypes} from "../../types/projects";

// export const getRoomProjectRequest = (id:number) => {
//     return async (dispatch:Dispatch<ProjectAction>) => {
//         try{
//             dispatch({type:ProjectActionTypes.GET_PROJECTS_REQUEST})
//             const response = await Api.getRoomProjects(id)
//             console.log(response, 'res')
//             dispatch({type: ProjectActionTypes.GET_PROJECTS_SUCCESS, payload: response.data})
//         }
//         catch (e){
//             dispatch({type: ProjectActionTypes.GET_PROJECTS_FAIL, payload: 'error in loading projects'})
//         }
//     }
// }

// create
export const createProjectRequest = (formData:{[key:string]:string | File}) => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type: ProjectActionTypes.CREATE_PROJECT_REQUEST})
            const request = await Api.createProject(formData as any)
            dispatch({type: ProjectActionTypes.CREATE_PROJECT_SUCCESS, payload: {project: request.data}})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.CREATE_PROJECT_FAIL, payload: 'error '})
        }
    }
}

// get all
export const getProjectsRequest = () => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type:ProjectActionTypes.GET_All_PROJECTS_REQUEST})
            const response = await Api.getAllProjects()
            console.log(response, 'res')
            dispatch({type: ProjectActionTypes.GET_All_PROJECTS_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.GET_ALL_PROJECTS_FAIL, payload: 'error in loading projects'})
        }
    }
}

export const getSingleProjectRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_REQUEST})
            const response = await Api.getSingleProject(id)
            console.log(response, 'project reducer');
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_FAIL, payload: 'error in loading projects'})
        }
    }
}

// edit
export const editProjectRequest = (id:number, formData:{[key:string]:string | File}) => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type:ProjectActionTypes.EDIT_PROJECT_REQUEST})
            const response = await Api.editProject(id, formData as any)
            console.log(response, 'edit')
            dispatch({type: ProjectActionTypes.EDIT_PROJECT_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.EDIT_PROJECT_FAIL, payload: 'error in loading'})
        }
    }
}
export const deleteProjectRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type: ProjectActionTypes.DELETE_PROJECTS_REQUEST})
            const response = await Api.deleteProject(id)
            console.log(response)
            setTimeout(() => {
                dispatch({type: ProjectActionTypes.DELETE_PROJECTS_SUCCESS, payload: response.data})
            }, 500)
        }
        catch (e){
            dispatch({type: ProjectActionTypes.DELETE_PROJECTS_FAIL, payload: 'error in loading projects'})
        }
    }
}
