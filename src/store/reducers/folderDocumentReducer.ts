import {
    FolderDocument,
    FolderDocumentAction,
    FolderDocumentActionTypes,
    FolderDocumentState
} from "../../types/folderDocument";

const initialState : FolderDocumentState = {
    folderdocuments: [],
    loading: false,
    error: null,
    folderdocument: {}
}
export const folderDocumentReducer = (state = initialState, action: FolderDocumentAction): FolderDocumentState => {
    switch (action.type) {
        // get  folder documents
        case FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST:
            return {loading:true, error: null, folderdocuments:[], folderdocument: action.payload as FolderDocument}
        case FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS:
            console.log( action.payload, 'documents')
            return {loading:false, error: null, folderdocuments: action.payload, folderdocument: {...action.payload}}
        case FolderDocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL:
            return {loading:false, error: null, folderdocuments: [], folderdocument: action.payload as FolderDocument}


        default: {
            return state
        }
    }
}
