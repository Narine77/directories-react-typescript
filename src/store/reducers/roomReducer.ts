import {Room, RoomAction, RoomActionTypes, RoomState} from "../../types/room"
import {ProjectAction, ProjectState} from "../../types/projects";

const initialState: RoomState = {
    rooms: [],
    loading: false,
    error: null,
    room: {}
}

export const roomReducer = (state = initialState, action: RoomAction): RoomState => {
    switch (action.type) {
        // create
        case RoomActionTypes.CREATE_ROOMS_REQUEST:
            return {loading: true, error: null, rooms: [], room: {...state.room}}
        case RoomActionTypes.CREATE_ROOMS_SUCCESS:
            return {loading: true, error: null, rooms: action.payload.room as Room[], room: {...state.room}}
        case RoomActionTypes.CREATE_ROOMS_FAIL:
            return {loading: true, error: null, rooms: [], room: {...state.room}}

// get
        case RoomActionTypes.GET_ROOMS_REQUEST:
            return {loading: true, error: null, rooms: [], room: {...state.room}}
        case RoomActionTypes.GET_ROOMS_SUCCESS:
            return {loading: false, error: null, rooms: action.payload, room: {...state.room}}
        case RoomActionTypes.GET_ROOMS_FAIL:
            return {loading: false, error: null, rooms: [], room: {...state.room}}

        // get one room
        case RoomActionTypes.GET_ROOM_REQUEST:
            return {loading: true, error: null, rooms: [], room: action.payload as Room}
        case RoomActionTypes.GET_ROOM_SUCCESS:
            return {loading: false, error: null, rooms: action.payload, room: {...action.payload}}
        case RoomActionTypes.GET_ROOM_FAIL:
            return {loading: false, error: null, rooms: [], room: action.payload as Room}

        //get one  project rooms
        case RoomActionTypes.GET_PROJECT_ROOM_REQUEST:
            return {loading: true, error: null, rooms: [], room: action.payload as Room}
        case RoomActionTypes.GET_PROJECT_ROOM_SUCCESS:
            return {loading: false, error: null, rooms: action.payload, room: {...action.payload}}
        case RoomActionTypes.GET_PROJECT_ROOM_FAIL:
            return {loading: false, error: null, rooms: [], room: action.payload as Room}


// edit

        case RoomActionTypes.EDIT_ROOMS_REQUEST:
            return {loading: true, error: null, rooms: [], room: {...state.room}}
        case RoomActionTypes.EDIT_ROOMS_SUCCESS:
            console.log(action.payload, 'edit room')
            return {loading: false, error: null, rooms: action.payload, room: {...state.room}}
        case RoomActionTypes.EDIT_ROOMS_FAIL:
            return {loading: false, error: null, rooms: [], room: {...state.room}}
        default: {
            return state
        }
    }
}
