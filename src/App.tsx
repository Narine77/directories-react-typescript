import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import {Home} from "./pages/Home";
import Project from "./pages/Project";
import EditProject from "./pages/EditProject";
import { ToastContainer } from 'react-toastify';
import {HTML5Backend} from "react-dnd-html5-backend";
import { DndProvider } from 'react-dnd/dist/core/DndProvider';
import EditRoom from './pages/EditRoom';

function App() {
    return (
        <DndProvider backend={HTML5Backend}>
        <>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                    <Route path="/project/:id" element={<Project/>} />
                    <Route path="/project/:id" element={<Project/>} />
                    <Route path="/project/edit/:id" element={<EditProject/>} />
                    <Route path="/room/update/:id" element={<EditRoom/>} />
                    <Route path="/create-project" element={<EditProject/>} />
                </Routes>
            </BrowserRouter>
            <ToastContainer />
        </>
        </DndProvider>
    );
}

export default App;
