import React, { useState } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {toast} from 'react-toastify';

function FileInput(props) {
  const [id] = useState(_.uniqueId('file_input'));
  const {
    label, className, children, ...restProps
  } = props;
  return (
    <div className={`fileInput ${className || ''}`}>
      {label ? <span className="label">{label}</span> : null}
      <label className="file" htmlFor={id}>
        {children}
        <input name={props.name} type="file" id={id} {...restProps} onChange={(event)=>{
          const fileAcceptTypes = [...(props.accept.split(','))];
          const files = event.target.files
          if (!files[0]){
            toast.error('File Does not exist');
            return;
          }
          const type = files[0].type
          if (!fileAcceptTypes.find(item => item.trim() === type)) {
            toast.error('Wrong file type');
            return;
          }
          props.onChange(files[0])
        }} />
      </label>
    </div>
  );
}

FileInput.propTypes = {
  label: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.any,
  onChange: PropTypes.func,
  accept: PropTypes.string,
  name: PropTypes.string
};
FileInput.defaultProps = {
  label: '',
  className: '',
  children: undefined,
  name: ''
};
export default FileInput;
