import {Document, DocumentAction, DocumentActionTypes, DocumentState} from "../../types/document"

const initialState : DocumentState = {
    documents: [],
    loading: false,
    error: null,
    document: {}
}
export const documentReducer = (state = initialState, action: DocumentAction): DocumentState => {
    switch (action.type) {
// get  room documents
        case DocumentActionTypes.GET_ROOM_DOCUMENT_REQUEST:
            return {loading:true, error: null, documents:[], document: action.payload as Document}
        case DocumentActionTypes.GET_ROOM_DOCUMENTS_SUCCESS:
            console.log( action.payload, 'documents')
            return {loading:false, error: null, documents: action.payload, document: {...action.payload}}
        case DocumentActionTypes.GET_ROOM_DOCUMENTS_FAIL:
            return {loading:false, error: null, documents: [], document: action.payload as Document}

        // get  folder documents
        // case DocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST:
        //     return {loading:true, error: null, documents:[], document: action.payload as Document}
        // case DocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS:
        //     console.log( action.payload, 'documents')
        //     return {loading:false, error: null, documents: action.payload, document: {...action.payload}}
        // case DocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL:
        //     return {loading:false, error: null, documents: [], document: action.payload as Document}


        default: {
            return state
        }
    }
}
