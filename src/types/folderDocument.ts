
export interface FolderDocument {
    name: string,
    description: string,
    avatar: string
}
export interface FolderDocumentState {
    folderdocuments: FolderDocument[],
    loading: boolean,
    error: null | string,
    folderdocument: FolderDocument
}
export enum FolderDocumentActionTypes{
    GET_FOLDER_DOCUMENT_REQUEST = "GET_FOLDER_DOCUMENT_REQUEST",
    GET_FOLDER_DOCUMENT_SUCCESS = "GET_FOLDER_DOCUMENT_SUCCESS",
    GET_FOLDER_DOCUMENTS_FAIL = "GET_FOLDER_DOCUMENTS_FAIL",
}
// get by folderId
interface GetFolderDocumentRequestAction{
    type: FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST,
    payload? : any
}

interface GetFolderDocumentSuccessAction{
    type: FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS,
    payload: []
}
interface GetFolderDocumentFailAction{
    type: FolderDocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL,
    payload: []
}
export type FolderDocumentAction = GetFolderDocumentRequestAction | GetFolderDocumentSuccessAction | GetFolderDocumentFailAction
