import {Dispatch} from "redux";
import {ProjectFolderAction, ProjectFolderActionTypes} from "../../types/projectFolder";
import Api from "../../Api";

export const deleteFolderProjectIdRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectFolderAction>) => {
        try{
            dispatch({type:ProjectFolderActionTypes.DELETE_PROJECT_ID_REQUEST })
            const response = await Api.deleteFolderProjectId(id)
            console.log(response, 'deleteFolderProjectIdRequest')
            dispatch({type: ProjectFolderActionTypes.DELETE_PROJECT_ID_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectFolderActionTypes.DELETE_PROJECT_ID_FAIL, payload: 'error in loading users'})
        }
    }
}
export const editFolderRoomIdRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectFolderAction>) => {
        try{
            dispatch({type:ProjectFolderActionTypes.EDIT_FOLDER_ID_REQUEST })
            const response = await Api.editFolderRoomId(id)
            console.log(response, 'deleteFolderProjectIdRequest')
            dispatch({type: ProjectFolderActionTypes.EDIT_FOLDER_ID_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectFolderActionTypes.DELETE_PROJECT_ID_FAIL, payload: 'error in loading users'})
        }
    }
}
export const getProjectFolderRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectFolderAction>) => {
        try{
            dispatch({type:ProjectFolderActionTypes.GET_PROJECT_FOLDER_REQUEST})
            const response = await Api.getProjectFolders(id)
            console.log(response, 'projectfolder action' )
            dispatch({type: ProjectFolderActionTypes.GET_PROJECT_FOLDER_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectFolderActionTypes.GET_PROJECT_FOLDER_FAIL, payload: 'error in loading folders'})
        }
    }
}
