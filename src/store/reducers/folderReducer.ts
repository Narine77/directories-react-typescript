import {Folder, FolderAction, FolderActionTypes, FolderState} from "../../types/folder"

const initialState : FolderState = {
    folders: [],
    loading: false,
    error: null,
    folder: {}
}

export const folderReducer = (state = initialState, action: FolderAction): FolderState => {
    switch (action.type) {
// get user
//         case FolderActionTypes.GET_FOLDERS_REQUEST:
//             return {loading:true, error: null, folders:[]}
//         case FolderActionTypes.GET_FOLDERS_SUCCESS:
//             console.log(action.type, 'type', action.payload)
//             return {loading:false, error: null, folders: action.payload}
//         case FolderActionTypes.GET_FOLDERS_FAIL:
//             return {loading:false, error: null, folders: []}

        // single folder
        case FolderActionTypes.GET_SINGLE_FOLDER_REQUEST:
            return {loading: true, error: null, folders: [], folder: action.payload as Folder}
        case FolderActionTypes.GET_SINGLE_FOLDER_SUCCESS:
            return {loading: false, error: null, folders: action.payload, folder: {...action.payload}}
        case FolderActionTypes.GET_SINGLE_FOLDER_FAIL:
            return {loading: false, error: null, folders: [], folder: action.payload as Folder}

        //get one   rooms folders
        case FolderActionTypes.GET_ROOM_FOLDER_REQUEST:
            return {loading: true, error: null, folders: [], folder: action.payload as Folder}
        case FolderActionTypes.GET_ROOM_FOLDER_SUCCESS:
            console.log(action.payload, 'roomfolders reducer')
            return {loading: false, error: null, folders: action.payload, folder: {...action.payload}}
        case FolderActionTypes.GET_ROOM_FOLDER_FAIL:
            return {loading: false, error: null, folders: [], folder: action.payload as Folder}

        // //get one   project folders
        // case FolderActionTypes.GET_PROJECT_FOLDER_REQUEST:
        //     return {loading: true, error: null, folders: [], folder: action.payload as Folder}
        // case FolderActionTypes.GET_PROJECT_FOLDER_SUCCESS:
        //     console.log(action.payload, 'projecfolders reducer')
        //     return {loading: false, error: null, folders: action.payload, folder: {...action.payload}}
        // case FolderActionTypes.GET_PROJECT_FOLDER_FAIL:
        //     return {loading: false, error: null, folders: [], folder: action.payload as Folder}

        // case FolderActionTypes.DELETE_PROJECT_ID_REQUEST:
        //     return {loading: true, error: null, folders: [], folder: {...state.folder}}
        // case FolderActionTypes.DELETE_PROJECT_ID_SUCCESS:
        //     console.log(action.payload, 'edit folder')
        //     return {loading: false, error: null, folders: action.payload, folder: {...state.folder}}
        // case FolderActionTypes.DELETE_PROJECT_ID_FAIL:
        //     return {loading: false, error: null, folders: [], folder: {...state.folder}}

        // // create
        //        case UserActionTypes.CREATE_USERS_REQUEST:
        //            return {loading:true, error: null, users: []}
        //        case UserActionTypes.CREATE_USERS_SUCCESS:
        //            return {loading:true, error: null, users: action.payload.user, token: Account.setToken(action.payload.token)}
        //        case UserActionTypes.CREATE_USERS_FAIL:
        //            return {loading:true, error: null, users: []}
        default: {
            return state
        }
    }
}
