import React, {SyntheticEvent, useEffect, useState} from 'react';
import Input from '../components/form/Input';
import _ from 'lodash';
import FileInput from '../components/form/FileInput';
import {ReactComponent as ProfileIcon} from '../assets/images/profile-image.svg';
import {toast} from 'react-toastify';
import {useActions} from '../hooks/useAction';
import {useTypeSelector} from "../hooks/userTypeSelector";
import {useFormik} from 'formik';
import * as Yup from 'yup';
// import {createRoomRequest, editRoomRequest, getSingleRoomRequest} from '../store/actions/room';
import { FormTypes} from '../helpers/interface';
import {Link, useNavigate, useParams} from 'react-router-dom';
import {createProjectRequest} from "../store/actions/project";


export const EditProject: React.FC = () => {
    const [file, setFile] = useState<File | null>(null);
    const [url, setUrl] = useState<string>('')
    const {createRoomRequest} = useActions()
    const {createProjectRequest} = useActions()
    const {getSingleProjectRequest} = useActions()
    const {editProjectRequest} = useActions()
    // const {getSingleRoomRequest} = useActions()
    const navigate = useNavigate();
    const {id} = useParams();
    const [formData, setFormData] = useState<{[key:string]:string}>({})
    // const {loading, room, error} = useTypeSelector(state => state.rooms)
    const {loading, project, error} = useTypeSelector(state => state.projects)

    useEffect(() => {
        if (id) {
            if (!loading && project && !error) {
                setUrl(`${process.env.REACT_APP_BASE_URL}/${project.avatar}`)
                formik.setFieldValue('name', project.name);
                formik.setFieldValue('description', project.description);
            }
        }
    }, [loading, project])

    useEffect(() => {
        if (id) {
            console.log(id, 'uId');
            (async () => {
                await getSingleProjectRequest(+id);
                console.log('hello')
            })();
        }
    }, [id])
    const formik = useFormik({
        initialValues: {
            name: '',
            description: '',
        } as FormTypes,
        validationSchema: Yup.object({
            name: Yup.string()
                .max(12, 'Must be 12 characters or less')
                .required('Required'),
            description: Yup.string()
                .max(20, 'Must be 20 characters or less')
                .required('Required')
        }),

        onSubmit: async (values: FormTypes) => {
            if (!file) {
                return
            }
            values.avatar = file;
            if (!id) {
                await createProjectRequest(values);
                navigate('/');
            } else {
                if (id) {
                    console.log(values, 'vvv', id)
                   await editProjectRequest(+id, values)
                    navigate('/');
                    console.log('update')
                }
            }
        }
    });
    const uploadFile = async (image: File) => {
        setFile(image)
    }
    useEffect(() => {
        if (file) {
            const reader = new FileReader();

            reader.onloadend = function () {
                const buffer: ArrayBuffer | null | string = reader.result
                const blob = new Blob([buffer as BlobPart]);
                const blobUrl = URL.createObjectURL(blob);
                setUrl(blobUrl);
            }
            reader.readAsArrayBuffer(file)
        }
    }, [file])

    /*const handleSubmit = async (ev: SyntheticEvent)=>{
        ev.preventDefault();
        console.log(formData, 'formdatadfg')
        const data  = await createRoomRequest(formData);
        // const { payload: { data } } = await dispatch(createRoomRequest(formData));
        if (!data) {
            toast.error('Invalid data!');
        }
        console.log(formData, 'formData')
    }*/
    return (
        <div>
            <button className="btn-green"><Link to="/">Go Back</Link></button>
            <div className="create-room">
                <form onSubmit={formik.handleSubmit}>
                    <FileInput
                        label="Add Image"
                        onChange={uploadFile}
                        name="file"
                        accept="image/jpg, image/jpeg, image/png"
                    >
                        {url ? <img className="profile-image" src={url} alt=""/> : <ProfileIcon/>}

                    </FileInput>
                    <div className="input-container">
                        <Input
                            label="Name"
                            name="name"
                            type="text"
                            placeholder="Room"
                            onChange={formik.handleChange}
                            value={formik.values.name}
                        />
                        {formik.errors.name ? <p>{formik.errors.name}</p> : <></>}
                    </div>
                    <div className="input-container">
                        <Input
                            className="input-secret"
                            label="Description"
                            name="description"
                            type="text"
                            placeholder="Room"
                            onChange={formik.handleChange}
                            value={formik.values.description}
                        />
                        {formik.errors.description ? <p>{formik.errors.description}</p> : <></>}
                    </div>
                    <button type="submit" className="btn btn-save">Save</button>
                </form>
            </div>
        </div>
    );
};


export default EditProject;
