import {Dispatch} from "redux";
import Api from "../../Api";
import {RoomAction, RoomActionTypes} from "../../types/room";
import {ProjectAction, ProjectActionTypes} from "../../types/projects";

export const getRoomsRequest = () => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type:RoomActionTypes.GET_ROOMS_REQUEST})
            const response = await Api.getAllRooms()
                dispatch({type: RoomActionTypes.GET_ROOMS_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: RoomActionTypes.GET_ROOMS_FAIL, payload: 'error in loading rooms'})
        }
    }
}
export const getProjectRoomRequest = (id:number) => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type:RoomActionTypes.GET_PROJECT_ROOM_REQUEST})
            const response = await Api.getProjectRooms(id)

            dispatch({type: RoomActionTypes.GET_PROJECT_ROOM_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: RoomActionTypes.GET_PROJECT_ROOM_FAIL, payload: 'error in loading rooms'})
        }
    }
}


export const getSingleProjectRequest = (id:number) => {
    return async (dispatch:Dispatch<ProjectAction>) => {
        try{
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_REQUEST})
            const response = await Api.getSingleProject(id)
            console.log(response, 'project reducer');
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_FAIL, payload: 'error in loading projects'})
        }
    }
}
export const getSingleRoomRequest = (id:number) => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type:RoomActionTypes.GET_ROOM_REQUEST})
            const response = await Api.getSingleRoom(id)
            console.log(response, 'hhhhsfgh');
            dispatch({type: RoomActionTypes.GET_ROOM_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: RoomActionTypes.GET_ROOM_FAIL, payload: 'error in loading rooms'})
        }
    }
}

export const editRoomRequest = (id:number, formData:{[key:string]:string | File}) => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type:RoomActionTypes.EDIT_ROOMS_REQUEST})
            const response = await Api.editRoom(id, formData as any)
            console.log(response, 'edit')
            dispatch({type: RoomActionTypes.EDIT_ROOMS_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: RoomActionTypes.EDIT_ROOMS_FAIL, payload: 'error in loading rooms'})
        }
    }
}
export const deleteRoomRequest = (id:number) => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type:RoomActionTypes.DELETE_ROOMS_REQUEST})
            const response = await Api.deleteRoom(id)
                dispatch({type: RoomActionTypes.DELETE_ROOMS_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: RoomActionTypes.DELETE_ROOMS_FAIL, payload: 'error in loading users'})
        }
    }
}


export const createRoomRequest = (formData:{[key:string]:string | File}) => {
    return async (dispatch:Dispatch<RoomAction>) => {
        try{
            dispatch({type: RoomActionTypes.CREATE_ROOMS_REQUEST})
            const request = await Api.createRoom(formData as any)
                dispatch({type: RoomActionTypes.CREATE_ROOMS_SUCCESS, payload: {room: request.data}})
        }
        catch (e){
            dispatch({type: RoomActionTypes.CREATE_ROOMS_FAIL, payload: 'error '})
        }
    }
}

