

export interface ProjectFolder {
    name: string,
    description: string,
    avatar: string
}

export interface ProjectFolderState {
    projectfolders: ProjectFolder[],
    loading: boolean,
    error: null | string,
    projectfolder: ProjectFolder
}
export enum ProjectFolderActionTypes {
    GET_PROJECT_FOLDER_REQUEST = "GET_PROJECT_FOLDER_REQUEST",
    GET_PROJECT_FOLDER_SUCCESS = "GET_PROJECT_FOLDER_SUCCESS",
    GET_PROJECT_FOLDER_FAIL = "GET_PROJECT_FOLDER_FAIL",

    DELETE_PROJECT_ID_REQUEST = "DELETE_PROJECT_ID_REQUEST",
    DELETE_PROJECT_ID_SUCCESS = "DELETE_PROJECT_ID_SUCCESS",
    DELETE_PROJECT_ID_FAIL = "DELETE_PROJECT_ID_FAIL",

    EDIT_FOLDER_ID_REQUEST = "EDIT_FOLDER_ID_REQUEST",
    EDIT_FOLDER_ID_SUCCESS = "EDIT_FOLDER_ID_SUCCESS",
    EDIT_FOLDER_ID_FAIL = "EDIT_FOLDER_ID_FAIL",
}

// delete project id
interface DeleteProjectIdRequestAction {
    type: ProjectFolderActionTypes.DELETE_PROJECT_ID_REQUEST,
    payload?: any
}

interface DeleteProjectIdSuccessAction {
    type: ProjectFolderActionTypes.DELETE_PROJECT_ID_SUCCESS,
    payload: []
}

interface DeleteProjectIdFailAction {
    type: ProjectFolderActionTypes.DELETE_PROJECT_ID_FAIL,
    payload?: any
}
// edit project id
interface EditRoomIdRequestAction {
    type: ProjectFolderActionTypes.EDIT_FOLDER_ID_REQUEST,
    payload?: any
}

interface EditRoomIdSuccessAction {
    type: ProjectFolderActionTypes.EDIT_FOLDER_ID_SUCCESS,
    payload: []
}

interface EditRoomIdFailAction {
    type: ProjectFolderActionTypes.EDIT_FOLDER_ID_FAIL,
    payload?: any
}

// get by projectId
interface GetProjectFolderRequestAction {
    type: ProjectFolderActionTypes.GET_PROJECT_FOLDER_REQUEST,
    payload?: any
}

interface GetProjectFolderSuccessAction {
    type: ProjectFolderActionTypes.GET_PROJECT_FOLDER_SUCCESS,
    payload: []
}

interface GetProjectFolderFailAction {
    type: ProjectFolderActionTypes.GET_PROJECT_FOLDER_FAIL,
    payload: []
}
export type ProjectFolderAction =  DeleteProjectIdRequestAction | DeleteProjectIdSuccessAction | DeleteProjectIdFailAction
    | GetProjectFolderRequestAction | GetProjectFolderSuccessAction | GetProjectFolderFailAction |
    EditRoomIdRequestAction | EditRoomIdSuccessAction | EditRoomIdFailAction
