import React, {SyntheticEvent, useState} from 'react';
import Input from '../components/form/Input';
import _ from 'lodash';
import FileInput from '../components/form/FileInput';
import {ReactComponent as ProfileIcon} from '../assets/images/profile-image.svg';
import {toast} from 'react-toastify';
import { useActions } from '../hooks/useAction';
import {useTypeSelector} from "../hooks/userTypeSelector";
import { Formik, Form, useField } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { createRoomRequest } from '../store/actions/room';
type DynamicObject = { [key: string]: string | any };

export const Create: React.FC = () => {
    const [formData, setFormData] = useState<DynamicObject>({})
    const {createRoomRequest} = useActions()
    const dispatch = useDispatch();

    const handleChange = (key: string, value: FileList | string | null) => {
        formData[key] = value;
        if (key === 'file') {
            const fileAcceptTypes = ['image/jpg', 'image/jpeg', 'image/png'];
            if (!fileAcceptTypes.includes(value.type)) {
                toast.error('Wrong file type');
                return;
            }
            value.uri = URL.createObjectURL(value);
            console.log(value.uri, 'value uri')
        }
        _.set(formData, key, value);
        setFormData({ ...formData });
    };
    const getAvatar = () => {
        const {file, avatar} = formData;
        if (file?.uri) {
            return formData.file.uri;
            console.log(file.uri, 'file.uri')
        }
        if (avatar) {
            console.log(avatar, 'avatar')
            return formData.avatar;
        }
        return undefined;
    };
    const handleSubmit = async (ev: SyntheticEvent)=>{
        ev.preventDefault();
        console.log(formData, 'formdatadfg')
        const { payload: { data } } = await createRoomRequest(formData);
        // const { payload: { data } } = await dispatch(createRoomRequest(formData));
        if (!data) {
            toast.error('Invalid data!');
        }
        console.log(formData, 'formData')
    }
    return (
        <div className="create-room">
            <Formik
                initialValues={{
                    name: '',
                    description: '',
                    file: ''
                }}
                validationSchema={Yup.object({
                    name: Yup.string()
                        .max(12, 'Must be 12 characters or less')
                        .required('Required'),
                    description: Yup.string()
                        .max(20, 'Must be 20 characters or less')
                        .required('Required'),
                })}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                    }, 400);
                }}
            >
                <Form onSubmit={handleSubmit} encType="multipart/form-data">
                    <FileInput
                        label="Add Image"
                        onChange={(ev: React.ChangeEvent<HTMLInputElement>):void => {
                            // try{  const fileList = ev.target.files
                            handleChange('file', ev.target.files[0]);
                            // }catch (e){
                            //     console.warn(e);
                            // }
                            //
                            // ev.target.value = '';
                        }}
                        accept="jpg,.jpeg,.png"
                    >
                        {getAvatar() ? <img className="profile-image" src={getAvatar()} alt=""/> : <ProfileIcon/>}
                    </FileInput>
                    <div className="input-container">
                        <Input
                            label="Name"
                            name="name"
                            type="text"
                            placeholder="Room"
                            onChange={(ev: any) => handleChange('name', ev.target.value)}
                        />
                    </div>
                    <div className="input-container">
                        <Input
                            className="input-secret"
                            label="Description"
                            name="description"
                            type="text"
                            placeholder="Room"
                            onChange={(ev: any) => handleChange('description', ev.target.value)}
                        />
                    </div>
                    <button type="submit" className="btn btn-save">Save</button>
                </Form>
            </Formik>
        </div>
    );
};


// return (
//     <div>
//         <div className="create-room">
//             <form className="input-block" onSubmit={handleSubmit}>
//                 <FileInput
//                     label="Add Image"
//                     onChange={(ev: React.ChangeEvent<HTMLInputElement>):void => {
//                         try{
//                             handleChange('file', ev?.target?.files);
//                         }catch (e){
//                             console.warn(e);
//                         }
//                         //
//                         ev.target.value = '';
//                     }}
//                     accept="jpg,.jpeg,.png"
//                 >
//                     {/*<ProfileIcon/>*/}
//                     {getAvatar() ? <img src={getAvatar()} alt=""/> : <ProfileIcon/>}
//                 </FileInput>
//                 <div className="input-container">
//                     <span>Name</span>
//                     <Input
//                         className="input-secret"
//                         placeholder="Name"
//                         onChange={(ev: any) => handleChange('name', ev.target.value)}
//                         value={formData.name || ''}
//                     />
//                 </div>
//                 <div className="input-container">
//                     <span>Description</span>
//                     <Input
//                         className="input-secret"
//                         placeholder="Description"
//                         onChange={(ev: any) => handleChange('description', ev.target.value)}
//                         value={formData.description || ''}
//                     />
//                 </div>
//                 <button className="btn btn-save">Save</button>
//             </form>
//
//         </div>
//     </div>
// );
// }

export default Create;
