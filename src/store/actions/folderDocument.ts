import {Dispatch} from "redux";
import Api from "../../Api";
import {FolderDocumentAction, FolderDocumentActionTypes} from "../../types/folderDocument";

export const getFolderDocumentRequest = (id:number) => {
    return async (dispatch:Dispatch<FolderDocumentAction>) => {
        try{
            dispatch({type:FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST})
            const response = await Api.getFolderDocuments(id)
            dispatch({type: FolderDocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS, payload: response.data})
            console.log(response, 'response')
        }
        catch (e){
            dispatch({type: FolderDocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL, payload: 'error in loading'})
        }

    }
}
