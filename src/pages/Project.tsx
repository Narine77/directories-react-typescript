import React, {useEffect, useMemo, useState} from 'react';
import {useTypeSelector} from "../hooks/userTypeSelector";
// import {useActions} from "../hooks/useAction";
import {deleteProjectRequest, getRoomProjectRequest} from "../store/actions/project";
import {Link, useParams} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {useActions} from "../hooks/useAction";

export const Project: React.FC = () => {
    const {projects, loading, error} = useTypeSelector(state => state.projects)
    const {getRoomProjectRequest} = useActions()
    const {deleteProjectRequest} = useActions()
    // const [refresh, setRefresh] = useState(false);
    const {id} = useParams();

    useEffect(() => {
        if (id) {
            console.log(id, 'uId');
            (async () => {
                await getRoomProjectRequest(+id);
            })();
        }
    }, [id])
    const handleDelete = async (projectId: number) => {
        try{
            await deleteProjectRequest(projectId)
            if (!id) throw new Error('no id');
            await getRoomProjectRequest(+id)
        }catch (e){
            console.warn(e);
        }
        // setTimeout(()=> {
        //     getRoomProjectRequest(+id)
        // },1000)

    };

    return (
        <div className="product-container">
            <button className="btn-green"><Link to="/">Go Back</Link></button>
            {projects?.map((item: any) => (
                <div className="product" key={item.id}>
                    {item.avatar ? (
                        <img className="room-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}
                             alt=""/>) : null}
                    <Link key={item.id} to={`/project/folder/${item.id}`}>
                        <div className="product-name">{item.name}</div>
                    </Link>
                    <div className="btn-block">
                        <Link key={item.id} to={`/project/edit/${item.id}`}><button className="btn">Update</button></Link>
                        <button className="btn" onClick={() => handleDelete(item.id)}>Delete</button>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default Project;
