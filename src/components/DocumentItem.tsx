import React from 'react';
import {Link} from "react-router-dom";

function DocumentItem(props:any) {
    return (
        <div className="room-folder-element1"
        >
            <img className="folder-img"
                 src={`${process.env.REACT_APP_BASE_URL}/${props.avatar}`}
                 alt=""/>
            <div >{props.name}</div>
            <div>{props.id}</div>
            <div className="btn-block">
                <Link key={props.id} to={`/project/update/${props.id}`}>
                    <button className="btn">Update</button>
                </Link>
                <button className="btn" type="button" id={props.id}
                >Delete
                </button>
            </div>
        </div>
    );
}

export default DocumentItem;
