import React, {Fragment, useEffect, useMemo, useState} from 'react';
import {useTypeSelector} from '../hooks/userTypeSelector';
import {useActions} from "../hooks/useAction";
import {getProjectsRequest} from "../store/actions/project";
import {Link} from 'react-router-dom';
import Button from '../components/form/Button';
import {useDispatch} from 'react-redux';
import {deleteRoomRequest, getProjectRoomRequest} from "../store/actions/room";
import Room from "./Room";
import {deleteFolderRequest, getProjectFolderRequest} from "../store/actions/folder";
import {getFolderDocumentRequest, getRoomDocumentRequest} from "../store/actions/document";
import FolderItem from "../components/FolderItem";
import { useDrop } from 'react-dnd/dist/hooks/useDrop';


const list = [{
    id: 1,
    url: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHkAwgMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAACBQEGB//EADsQAAICAQIEAwQIBAYDAQAAAAECAAMRBCEFEjFBE1FhBiJScRQyQoGRkqHRcrHB4RUjM2KT8IKD8Rb/xAAZAQADAQEBAAAAAAAAAAAAAAAAAQIDBAX/xAAoEQACAgIABgEEAwEAAAAAAAAAAQIDERIEEyExQVFSFCIyYUJxoSP/2gAMAwEAAhEDEQA/APkBS2v3WG0vSORslcgjBEaNtGp+zWh/25A/WKMSje6GI8jNkbyiovKeQ1dVZYh25fL1hSUAw3MOw5ovW4xuB8jmFDM6YIHzg02VGSwWOlW5cry58wYs9FlR+sCM+fWNUi3OazysO3aWe1nOLlX58u8EmOUYyWezFKhhg4wTnpCgVcx8QYBPUCc5QXJQbGXqqeywLyx4Iin2L3BWQqmQo+rk9YgyEN02M1blfT5UgMNgR5QJVXU5IDeQ/pBIqcMnU05s03ig7rgEeQ7GL8xtfkI26Qoeys4XmG2D6iHq0r11eORnBziAa7diukGQVbociVavwXB5SV3Ec0KVWalF35N+ac1dQBYIRjr8z+0XZm3LzXky9g+3eEcAoGE7ZVgqwGPP0lkwEIPU9JRzpeGJMN4SlebOeghtRXymDr2rJlIyccSKMN5RxuIVRzE+kqwjJBhOZ/SEYYXlX8ZekIDmzpO3FXbCLgZ6QFgtoqgCzkj+vnKay03MT5sTJ7yFVBOcZ+WROtUThR/8ktGqeY6oX8MTsv8ARm+MfjJJyLX9A+TkO5yJZAyYOP1nUIxjqIbKmvlxuDkHyjBJPsWWtmQsPq5xOplNh9U9ZVLTjl3xGqALFYnquO0DeCT7BEQAKx6GUZEewgkgdpx3IGBCV4Nav3zJN1hvBWvTFH5s5EMhy3uruRiGpvTnwy7HadWvls5huB0k5NVUsfaSrTrYDztlu8FqqUFgbTnGNx6xhkJYFGw3yndOlaWFH2I6R58luC/H/ReqhQVNy4J3wZW6xjaEUe4CQCR1mhqkNtgtVcHGMeWItqNO55GQe916wUhTpcV9pe3TCrT1NTklySx8sdpyytG0wcHJAjGiTl5kuOA2GznoYXUVpQpBXOGHvdt4m/BtGta7GRRWCti2DJK7QT0chHRvLHaa1mlcKl1QVXzErAPpHvAg9/6ykzlnVhJNCWrHNXlR2ibZVQs1mVV5kxnHQxfT6U6jVLWBk5zj0lprBzWVOUkl3Ymi8tZ/3ShWaPEalTV2qgwvNtF1qDVuxP1cRp5WTGdTi9fQm0vV9YTpXOw6y4qO2CMyjJLqRE5nDfL+UsKyambcBfPvDU1g2+8p5Ap3HQwGotUO3LnuJnL0dEUlHLFCd+sk5tJFgx2RcIR2lx03lVLL0P4wgZftLv6S8DR1Bn5RmmsqTzEqR5wKqCfcOR6xmtyycrnOOh7/ACg0awwiXVuqB/rL8Qg6iRnEc+mNWgXlKjGG8mA6QdtVVgNtD8ozuJOPZq0s5iFrsTlBXqOsc0dp5yGTKEb4EyS5r+yD5Ex7R3hUAB5T13kyh0N6b1t1HLWNOuBwrIwzt2h1ro1SkhssG6dIhawsCkN7oPUTp1j1VYAGB3A3k6NrodKvgpPbsO+INO4RlJXOM9cyuuVazUcO1ZPUSvD9TTfldQBvuDjcGPazSs2mzUObByOUdJD+2R0R/wClbcWDt0fj45GXZduX9oO+p/ozVWElgNv1jFHJVWLtRWA3TPP0Hyhr7+ZEa1P8phjIOSYstMvSDi89zP0NllqIioLFQe8X7f3iWr9y/n8j37zV4fpwPGevTrYndlPvftKcQ4fZYVesEg9M9QJTklIw5Up09O5jIr28wQHGdsDMa4CvJxY8y/YI++dVBQxDE8w7ZxL6UKuqrt5iPe970jk8xaMqqtbYvymKa6tn1lmBncxPWWKD4VeeUbn1M3bNNz22sh+s55fWYuppHiZHXvNK30Obiq2m5LyK1VZJM61R7HeGDKqwTkkbRufg5lUsdSeL4VXIDlgdjEnyx36mMFc5J6wbL6RrBnPL6C/LJCcpkiyjLRhOT0nQk0hpc9v0nRpD5R7I6eSzOCQihgMdvWPDSnylhpT5R7IFVJCRBZcHpOoGXIGRnrNBdKfKXGkPlHuiuVIzgM9RIUz22mn9EPwy30Q/DDdByWZ9bWVjCnYwoHiUtkDP84PWanT6UMrMGsH2F6zNPFbSM1oi77E7yXNEbavDNJAa2yp7b7dJvcL1KadRcW5U+2pbZjPCXavUXsS9rH0GwEFyu3XJB85lZYpIum+VbzE9lxG1bLW5AUOd18oCnUXIjVc2UfrkCeV5GLZ3z85ZHuU5V3BBznMI2rGBTvm5bHruF6m+i/KHAPU5GJo6ri3IEQZdk+0J4yniV6EeJyv6YxmaOg4lpbvd1TeC/b4T9/aNuEnlm1PFSitYvB6ym2njNR8SpRauwIOD94iycNdLuQbhTnpFqqXrIspJHcMDNjTa5nUB1y4+qRMJ5j+PY9aiyFmOb39+zJ1AJ1TJW+RWcHtjHUzIvYM7FenlPSajhj1132qwLWHpv7vpmec1aGsnmKnPltFCzJPE0arIktbWPyqO8LZT4Z5Sd4bTFVGUDcx2zDGqtCWYF2752lbdTkVK0/ZnmvPvdvKCYeQjdxY7KAPSD+jkDLNj75pk55Q9C2D5SQ3hr8Q/GSGTPRnpV0/pLjTj0jYUToTfYTDdnqxqQqNMPKXXSjyjQAhFXyg5stVRFBpd+kMmkHwxpFBjNaCZuxlqqIkuhU9t4LX01aPR2X3FEVVJy7coz23m2qYHQRHimip4npH0mpBNbEHI6gjuIlY8isrWj17nylUa4vc7r1JGTuT5Dzhq6NM+AbXBPTK9Tttgbmeo417M3VamuzhmmVqVXccwLE9Oh2iZ4FxW2/N9DMWIBs90gKB5Dy89j6TfdM8B0WRlhox24XbX2zsSceXz/wCmN0cLdlIbAC9fOFr8bT2PprMhgeULYmGA36ZntvZbhicTZNOrNzWEl1Y7HuJz226nfw3Dxksy7HitVwd6rGAUry9Q+AR6dYh/hljkfAW69Af0n1f2l9nV4GAmofn5xluQ/W8sffPnupsuWx6qSwdiACE5nJ26d4q7cvDHdw8NN4djFu0tFB8K12FudxjYdYBqFdGZGxy9Eb6xE319m+MW1ll0FmSOZSx5CT54P8jiGb2R4/rHU2VopXcvY4Oc+WPKdG8V3Z57osl2ix/2Uv8Ap/DUqNitZVs2+SB2Jm4mjywWpMv2MD7N+zX+D0MXYWamz67gdPQTerD1WB1XcDuJlK3r0PZqqkq0prqItw1lU/SLgM9QDMXWcP0S2m0MHK9idp6bVrXqGDXc2cYwCf2mdbXp02rQEeRzOSV0mz1qKalDtn+zy9tOTlAMn+UJVor3GErH8U3Cgz7iqD8iZZUYHe4D05ZorsEz4dN9EYy8D1L5J90em0FqOC+GP8yz9ZvWZK4Oo29Jn6uqojfUOfliVHiDns4SOOhinQU56/rORo6TT5/1LP8Av3STbmnF9KzGHtHxDyo/4z+8sPaPiHnQP/Wf3mMJcCdjivR4sbrPZsf/AKLiPxU/k/vLD2h4ge9X5P7zHEuMRKCLV1nyNpPaLiQ6NT/xw6e0nFOz0/8AHMJTCowHePlw9Fq+z5M319peLtt4lf3ViHq41xNjnmT8gmDWe+SI5TZgjv8AdE64ejorvlnqzdr4txRhgvXj+AQ9Wv14OS6fcizKqvU9XVT8LNj9Mxmu717dpk60vB312p+TUXUWXoarlrZGbmYcg3Oc52HnNvR6z6Np/EREU1KzKQMEHE8j9NSp+ZmCjuSYW32i0VdLIdShYqRgH0nJbBS6YOnevV5weo0PHr+McNp1+pCWXWBhkrnAyRgfhEzq7tMW8CmlS3Uhdz8zPLey3HNJpuD06bU6lUevm2ZsdWJ/rNheJ6PVA+BqK3+TdJMKkmVVOp1rGBizjfEKxha6fwgG9pOLJnCaf8n94nqLdjuAB6iJPeD3E6o0xfg57bUhy72m4qMnkoz/AAn94nZ7V8WHaj8h/eJai7Pf9Yha48zNlRX8Tgt4qfiQ9d7RcTc8xNWfRT+8Xbj/ABHGCasfwn94i7DzECzCPkVfFGL42/xNmgeO67uavyn95RuP649TV+U/vM8kGUMXIr9EfW8R82Ptx3XH7Vf3LBPxjWuN3T8P7xI4lI+VD0Zy4y9/zY3/AInq/jH4H95IniSPlx9GX1N3yZ3OZ0SmRIbFUbmXkxCgywzFDqG6Lt6wbWO2xb8JG48j/iKv1mAnfpNY6uMfjM2SLdj2ZqDXUj7RP/jDV8UpX4sei/3mLJDdjU2jfPGaVGALemMA4gbuPajJFGEH6zIEklts0Vs/Ya3VXXtzXWM7eplQ8FiTEhonZhvElRaVOVJB84PE7DA9mPU8X1tW3jF1HZ940OOZH+ZUSe5B/pMfMhlp47D5sl5NZuL1Ef6bD7hBNxCpvjH3CZkkrdmbmzQOsqP2m/Cc+kVN0f8ASISQ3ZOzNDmU/aB+U4dukQBIORkHzl1ucdTzfONTDI1kyuYMXKeuxluYHoZWUxFuacnJIwFySe85JJMAJJJJACSSSQAkkkkAJLA5lZ0QGjskkkCzmZMzplYEtkzJJJAkkkkkAJJJJACSSSQAkm/nJJACZb4jOzkkAP/Z"
},
    {
        id: 2,
        url: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHkBQwMBIgACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAEBQIDBgEABwj/xABHEAACAQMCAgYECgcGBgMAAAABAgMABBEFEiExBhNBUWFxFCIygSMzQlJyc5GSobEVNENigsHRBxYkNVPhRGODk7LwNkWi/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAIxEAAgICAgIDAQEBAAAAAAAAAAECEQMSITFBUQQTIjJhQv/aAAwDAQACEQMRAD8A2aQDODwq30TPsycPE1BJg3tCrkZO811HMVmyYDIkX7aoYmM4JzimClKsAhPAqONKx0K47kdtSllR14nFMGsbVjnb+NQfTLaRSoJWlaCmKt8I4cD76ujihk9hsVadAQezKT76vg0TbynqtkTqypYNnJ6JhAUglg3hRcWh7hn0j8KoutIlgXcku4VFovVkpktCCZAp8qWTQxIwMe0jsxRkNncSfsyfOiFha2dest9wJxyp2kKrPz7rNy2raxc3ZJxK52j5qDgPw/OqUSKMEBRnvzRWrQw2WoXdrb/FxzugJ7gxA/Kl7PnJrFnntuTou61PmsfIV5ZovlRup7wKEaeJfalAryXkBOBNxosf1P0afoXbwx9J7e4hlVUlDRyKTgcRw/Gvp8i7G24zXxa3kLFXhk+EXjuXnX23o9fwaxo9teOmJHXEg7mHA/l+VawZrhk/5fZ2ArniDR0Sq3ZUoooI23cCCcURIgX2RgU2zpSOLHGBgc6rl2ngeyqnLBiQcVF1cgHjx8KdA2XxmNOyoTSrnIoZ2YdtDuzE4JooTZbLPihJLrB51CUMeWaHNrO54Lw8auiC03uO2om+8a4mlTnizADuq9dNhA9duIp8ByDm+NVvdM/bVs1qik7eNVeinGaA5BZJvE1Q0xol7ZyfVWvCyfGWFVZNMBaUnkaizN20yNiNgYKT7qjc2owCFPKiwpipmqppMUa1q5HsGh5LcKfhGCjxIoED9ZUWc0SbdAu4uNvfVK+jnOJFyPGgCgue6olyeVGhIiMhga8ohz2UABZevU0Cw49mvUAPY7r94Vct4E4lgfCsbbyXchCorknupqmlao0fWAr9HdxqGkUm2aAzHmjkqa4bhwMhqRQ/pG1GJYWx3ZzRCX4PB1Kt3GiirdDmC8lzxJ99GR3+ObcqSRahEuBIwHnUnntJgVFzGhI57sYpalKQy1PXrTToetuZQoP41ZperW+oxdbZ3CyDmQDxHmK+Wa3YakzM08wmiV/UYMOVBaRqV1o9yZ7ZsORg55Gn9aon7D7jDqM8T4IJU9tRnubu7fbGQqd5rK9Gum9neFLbUUW3nY4DfIc+ffW7itlCg459xrKS1NYvbonZzm3QddJubtOar1rWRY6RfX0aiR7eB5EXvIUkVOSzQgluAPeaUdJLeP8AQGo28TBp5LaRI4gfWZipwAKzcl2y6lVI/PN5OVZ5pnLSOSzE8yTxJpW8k0/IlVoq+0/UmvWS4sLuMg8FaFhy91euLK4UKqwSh8ZK4wfxqWTiwKKuXYAIlHtDPma6I0Ck7QcipzQXEce+aIqo5kkcKpDEEHmvnQbcIuhYoQ8DbSOPOvpHRjpaLPo1Lb7C8/XnbjgFyBn8q+aRsqn2eB4HjTPR3YSyxnkU3A57jitMX9o5/kQWu67NsvSfUbVV6qc7y2454im+mdNbsTma9kJjA9jsrERKztluQ/CvTZYY3cAa7nFHAskvZsdT6cek9asCkI3BePKgIeml/Db9WHZnzwY8ay5HHIFeINRSKUn7NTF01u5Z1W9kdbfiC0YG4CnM2v2wt82F6027jh1wV86+dlTXRuHImiirZrpulNxGQdx41quiXSKPV4zBIQtynZ84V8oBJPHJx31bbySW0qzwOySIchlODSaTRUZNM+5sD51S0O6sVo3TZpI0g1BfhgOEg5NTm26R28spG7GBmo1ZrvEdNbKR414woFxSyXpHaRjJfPvqqHpTZO2C2PGimG0Rx1SjsGa9lV5qKrW5jliEsT7lIzkUK18NzZ7O00UFobxvCV27BSnWby00+MyTkDuGeNIdY6WQ2YZINssvZ3CsPqWrXOozma4csx7OwUJA5Ifav0skwy2qbRjmayt1qlxdyDrJT9tUSsSTk0MyjOaLEh3a3qpGY7jrHTs2NyoJ7sF2wzqOzB/OgzKwTaDwqtiaG2NIcafqOyQB5SRntNPJLzTcKyzNvA5CsSMjOKmsrDHdTUgcTZDVzjhyrlZUXTeP212jYnU2lt0ygjCrJasGHNk5VbL03jUH0eFvNqwoGM17iM4pDs1B6aXhdmKKRngCaF1HpTeXwwBHEe0oONIOJNSC5NMVhLXk8h9aZz764shJyXb7aikHzq5sweFMmwkTybdokYjuJqSSK3F6HVSOVdCmqRDkGgxOw48adp0j1qGFIYtRlCKuFyeQrNqh8qJtoprqeO3hUvI5wq95p8dsm30jWaTrOtNZ3O+8eRWwqhzzcnvpR1t9qLzs168roHKsrbQTg4IB7M91bO00rTdM0GC31RmJZS02wHG4jsPPhyFQsL62jijttN6N362CLgSiM8AO3jXmTyweRuSs9vHhnHCoxdM+Y6hN0lv9MgNzcX8iyDci8QFIPHGKz01y8OYriBWkDHIkU5zX1rpbNe2kfp+jyQzWIBMqOhEsWB3EjI+zFfNNT1Mtd9YsETX0gBkkwTg/ug1cdWriZTUk/wBC301lRlWC3AP/AC6O0vozqGqP/hoHWLaGDuAFPkSRXDfXVu/U6jHFLFJnKlQMZGOBFMNQub9baFbJJo9OjKGKRIzgEDkWHOnaJ5FE2kXNtqQsLqCWOXdjaRxI7CPOn8enxafqN5CSxMT9WuRyA413RdTuru8tpL2VXK3ewlkAIRgTw8N2ftpjqsvpWoTTDBDPjgOeOGav46vIZ/L4wr2wV5FCgIKq6vcuRyqbLg4qeQE29td55SKOr2io48RRZT/Dr35qHUYhDHG4mpZQKADyzXWAAHjRMcfqkY4nlVcsQDgDkKRVg5Wq2zyzir2G3sqBAPMY99IdlQdxyYiiEupCykHaQMEjtqrZjs/GuBfdRyOwzrZZTxY10Iy7WEhyefhVMcmOBxmpmQfPNOxUNLTULm0bK3J2Y7DwqjUNdubjMcbMi9p76WySFuBJqBJxjuqW76LSrsgSxOWPGonlzqWCedcK1NMpNFT86qI40SU8Kj1R7FNFFWDEV7bkUR1LdzfZXOpfONjfYaQWDFa8Fog28ufi29ymui3l7In+6aRVg+2vUV6NN/ov901yigs0n9zb48Ovh/GrF6EXh53MP2GvokSQHk6GiFhixmix6o+cJ0Fuj/xUX3TV8fQO5z+txfdNfQQIR8oDzrvWW4/aJ9tGzDVGJj6DTHndR/drq/2fyE+tfL92tws0A+Wn3hUzdW6jPWJ94UtpBojDr/Z4x/8AsB9yrk/s7HbqB+5Wqk1K2Un4aP7a7Dq1s37eP7aLkLSBm0/s5jPPUH+4KuXoDHZqblL5y0QLAFQK11vf278po8/Sq+6kjNlMd6HKEAZ5nHConOSi7Lx4o7KjLwa5LqlhMti8Fo0bbYxLwBHZx76w/SDpd0l0uSMXNowRWyzr6yt7x2U21bqLeQH0cusftbWwazt50tuGuBbi2NxbE+oGXD8OwgV5iXPJ7DfHBo9E13T+mTx28qvBKyMJGh7eHDdzwM4/Ltr5hrWk3ljfTLNGeticrIFHLuYd4Ixxrc9Ebmyhubya7h9Fjv7QlJE5YU5H/vhRtxq+j6jYiPXfWlUkRSxNslUefaK0jNQZEsbmrPlSwz3bAENtHNmzgDvrS9GulGodHpnghYTW02GaF1yPP8K0EOi9GLyUdZqeoSIDnqJSAo94Ap3JonRK3h6pR1buPjM5b8auWWDVEY8Mk7M10m6QPrdrYv6HBAYrpVV402k5R8g/YD7q31j0O0CW3jcSyu20bvX5HHbWF6SRpp8FtFE0TxelAiRU5jY/MHkadR6hMJFaNsB4YgWY548qiPyPqp+BZPjrNaZqD0N0HIzu/wC5Xf7mdHs7juJ+tpe+nW9ycvLNG54nq3GMeRHnyoZujrTbvRL+QAf644faOX2GuqPzMcvJwz+Fkj/wh4ei3R1B6ycPGWq26PdGSMEKR9dWP1TTr+yjHpT5U8mR9wPvpb6+Mnf511KpK0zkf5dONH0I9H+jKcSqcP8AmVS2idFc5KRDx601g2f5rv8AxVZ1ZdR8OF+kKer9hsvRtTo/RUfs4T5Sf711dI6L5wIIT76xitJFxW6h+7Vy6jckbfTY0UdqrRqwtejarofR1hkWsOK6NC6Pj/hoKydpfXgPqXUswzz4AUzivrxiS8Rz34BpaspSXobyaR0fi4tb2w8ailj0fY4WG3J8qUvNJJ8ZGD7qvtkVuJhb+EUUF/4NxpOjtxFtB9lQax0VDg29tnxWowj1fVhf+ImqZbLrXyyED6RpFkng0WPj1Frx/dpVeaho0BKpFa5H7lEXmigDehkz9LFIbvSo4yXkEmfPNUiWdn1+0UfBWtsfKOhJekaDlbW4/wCnQVzbwAHJl+5QJhi73x44oskcf3l4cLeD7lUN0mnHK3hz9Cl/Uep6mPfiqGhbvpDG46S3Dc44gfoCpN0jus8Fj9yUoS03cdx9y5q70NSBl2+4aBjH+8d381PuCvUv9Cj/ANRv+2a9QBubaDYRxkHH5MTUzit5HXCSzDPeho6CzmQj/Eyt7qLFlI44yy58Dios1UWIn06YMQGkbP7lBT6FfyHKq3ngitSdMk/1JT5ua9JphZMMZT4BzRsGpnLfRrwKA0QY/vOf61ZJot/zSKMeAbP86cfoSEHJt7kn604/Opfozb8VZn+OQ/1p7C1Mjd6fqtuzOxCpjIIxQUdtqN6PWSR171TnWxutHnl3BbG3YH58zVXaaVd2il57awjhQEk72OPdT2pE6NsV2Wj2tvama/gmQDs6wrk9wqjTXkuOkUSmbZapE7Rw7uCnAAz39vM8+NV63qhly2xFi5BFXGPGsnNfy28vWL6zHtb5VeZlzvJLjo9TDhWJc9jjpNHJDM2y6HIkkYxSK3mKWd+2A04gfax+SMHJ9/Kqddvp9RtFSIxp6uH9flXrVp5PSIBAjPLhXkZyNuRjGAOPE1DTpM0u2wfTWkmsNNVpzGiFtzZxhFB/DjXHS1upisN1CwB4MpyP9qVNatcWcEURbIDZXs5ZrtvYG3mh3H1nU5HaK0kl7Itp0aK3ubSzDW19bRsxHqypjHmTR2nXcU0LWN4yPCTmJjxI8AayCuesMbZ3Byox20dp884bdbqrqDknGRUSgzRTNlqFlpw6PbLuZ1brkNsQMnfxzz7NpOfdVV4j22mQ59frFBBTjkKfD3Usgjl1C4in1Mho0GI4uw95Yd351ro4bPUrFtPndhG4IWRPVZD4VjkaSSNVyuCMd4rWyPHIGWQbiVPLw880NqOot6SY5JertoVXdt5sx48vsrk1kbCa2sQPVj7R291K4tak9OuQtpFLtkbq5T7W7Pb2AAY41ko+RuVcDy2N9ewZlQWmm8sSKWkkHl2VXLp+mqCv6WuSueQtW/rU9Nl1MMZHlkUSfGTv6qAdyKePvp81jcarb77fVVghRQjh14sR8rn/AO4ru+Dl1eng4PnYdlv5Mg9tpkTfB387kd9viikW0eLC3Eh81xTCToyBKrDWLSR8j2lzn8aNGh3GwKt9YLjt6v8A3r1NjzNTNS6bBJxF3GvmrUXp2iwlh/joW7x1BNOG0maP2tYs18oh/Wr7a0kVsR65bE9oWEUnIagXQaRaKgwqMfndURV40+BG3AE+AjoiESxja2pwsfGICrSs5PC/TyEVRZrrQKLdCfVhPn1f+9TSGZfYVx/0x/WrXgum56i4HcsQql2MB2zalIT3FcUnINS4RXTd6+aCvG0uDxMv/wCB/WuQTLID1M0kmO4VyRJWB+EdeHyuFLYvRlctnPt+NY+AjB/nSW+tJHyrJcfwxAfzpmxIHwlzISOxHzQJvLCRWMkt5GoOCxY4pqZLxsz1xpQwcem+WFpRPpkolwElK97YrYySaPIPVu589wY0mvbnQYpdstzcs3aBk0/sj5Yvql6F8WhtInElfMiqm6Ov23UCeZrQQz6RLGBCGOeW44qs2UjzYTTx1R4q28Emk8iKji9iZOj1wOVzER3iSpjRdp43W0/WU7hgkiB6zSiYx8rfigLyZpnItYbWFeWXkB/nRvQfXfRQNCJH+YQ++Q16uLp+oEZEtnjz/wB69R9iD6WfSYrGdfaumPuq/wBDcjjcuD4VBWlPOU1MMAfWY+Waix0R/R7Zyb658gwxXm04sMC9uvvj+lXBh2ZzU1weZNFjoB/RE55apeDycf0qa6NJj1tTvSfrB/SjcqBzNc3cMjNFiSAX0JGBLX974/Cmsl0llgsJDbWlzPIWIDvLIW+zwprrWtRtuitmYbTl23Hj4CsPfzGeYtuz3+FcefPf5R2YcNfplF5dShmV2AUjHiKQ3JY55n+dG3sgJJLce2lUspOQwIbkM91ZQibSZRbWvpWoiN+Sje5UnhjkBTmxZ+qu5UyXwWHnS/R5BvuJOTNwHgKa6ey29rLKx5A8+2nN+BQQo0eYGKI8dwVtx8Sasm/XY5HOQoJz4CvQBVLMqgZOcDvoa5ja4YxqSvqEk1XbJfQG5M1yRjqxIxPDx48fCm1sy2ccM1q5MZ3KSB8YwP4Dw7qTpLmxEaAhnbEjHtUclHh2mmlohNgsADMsmSSOwntrSfBK7GtpM8wMk5xx4jP2VoNOuIsBkPBeyshYMrKpKbH5HDcD2cRTiBsY6lwB2Z7TXLOJvCVGyiubfVDHEZurvUXEYK8HHdmstp4u0u2sotPENwvD1xlh+8Ty49lNNG22t5H1jK8j4JYDl4Cn95As/UOhPpMZJQ5wGHzT4VjtXBtrfIBDcT2TGCZJLot+zKeqPNu33VoNIjtLvEdxZxGHaWI4gq3dQEF3cOpE6SRkZBJUAjxqdk1x6arNdwSRA5DqQD9g7aeObjJMnJBSg0O2tOj0ZANvBn6Neez0ZlO22Tb3hDTONxLGkgQEN24HCuO/VqS+1VHbXsqSfJ5LhToSNpOhSHHoaknwNF2Wk2Ft61tbIp78UHcdJrGKcwqZHcf6cZNE2OqJehjHDOgUZy8ZGaNvAasYmFO2Jc/RFDXRuogPQ7aJ/pPtodtUHWdWILgnBJ9TgPfSx+lljE0i3CTxFDgll5VEppFKLPXera8khSLRlPH2ut4UP1vSi4dW9Fs4hzPyifCrj0z0UyKnpDcTgN1ZxTe3vbW6UNBcxPu4jBpJp+Ru14K7SW7KqJ40jbtx20YCxGGcEd1UTSQKfXuIgf3mrwMBTd10ez52eFXx5ZPJXcptYN6IsuDzBAoCVpYQzjS1O7l6w41z9P6Otx6OL9DJ24pNrfSe1tzJFZ3crTfJfaCgqZTikVFNjNFEsTtLpaROBwzxBrLajZ2hvN1xHBAp+YpyaAuelWsTblNzsjcbSVUYpTLqV0JlW89dR8vJ4isJZE+jaMWuzSCewSMW5t5JwvEAerQd1dZISOCeNs+rmUjbSW7vTt3QMVweZNU+mNOqvLI248AxOcVKbodcjW5ub+S36ma4cRnmhfnSn0C1lzvlMQHHnREgUbS05ckcBQNzvVigXII5VKlL2FIod0jYojSFRyO+vVSwXcc8D3V6q3DVH6MEAHYc+ddYRxgs5CgdpNDLdxAe3+ND3gtL2MxTjrIzzUnga7DkO3eu6ZZIWmu08lOaVz9NdNCj0Xrrg9qxoeHiTUl0PRBgLpsXDjxFMYRbwLthtIkAGMBRU8lcAFr0p663Mv6MvMgcAU50FcdKr2RJVezFp6p2bidx8a0DXIhjaVtqoqktw5Cvm+qanJc6tLO5J+CBwT3nNY55OKpM3wxT5aONd5eXJ7BknwpGLgiTrQ3HkQancTfAyHOS1LGkEaks3Ht/pXJFHTJkrm4OSFbiTxPdS6U7MvggAZJY8a6zdY5Lc6qu5AsYVuLH8K2iqMpPgnpkrbSwHAnNM5JzJCsYGFzx8aV6anwQpiF4UT7HHo6pCqapmbq7a4lBwdu0eZq3HfQepybY4414fKogrYMEhiDBU7qfW+Y1UZxwpXpse+Td2BackcF8qeV8lAuzqZHdTlHbJ8CaLgcnJDYUdlUiZYUmQpvWQAEk4wQcg1TExUYJ86zatCXY/imECoyHcc5yTT+O+dt07YwAMDsrFrIGQIOX5UWl1Js6ncQo7R21k4Gqkb67gtdas7d5HdZVYFGjkZGDY8DxB7qTNcxaZqJtb1Jra6UgxXbnfHMvZuOPz4il1ldSR2vzdsilTnxp7cXI1K0mhkVXYKD1bcFcYwVPgRwrPrhmi55Npotws1irAbWQ7WXOQKLneIENLJtA+ccV816M+mRwSWHpxdF2y2srvtYoeSt40z1eHUoIUl1OSOWHkFV8k12Qy1HqzjnjuV2aHVb6zssTL6OztzJfBHkKEt+k2nSErHC5cZ5cc1l7K7sJZ4xPZPImT8rGKddToEEyz+igbwTwYkqe6j7pdi+tGgtL21vYjI8ckagZJkXFL9VuOjjxvJcLBOe0JxJNKLjpCqo8VkWiY+qqlQ6/jQEmuI8PodzZwygdu0KfdQ8ya5BY6Yj1F4Z7kmHTxHbKfVweNaTo3qWmLD1M1rJbPnaZWGVb39lJrqVYJD6PGNzDbtJyMH+dDXslzDH8NOduMqg5D31nGdclONm71MdHQhuL6SAlh7W7i3urEarrELsI9MgMUK+qwY5JrMyXBu1Ek5C59h+3NG3FzHAAnXb2Kg424we+tJtvklJIi96YE6tm2uOIG0dvfULa6guI5VuAVfkCvf30sn4yGRncljxJAq7gcdUpXaBxHaaThwNMKKvFbjYd4HPjgmvO0dyqhmZGTgCV4UPPKJE6uJOrKsGVsnPjVMchjL5LZznBNZpMYZtQQtEV6wZySKuSezRAOrdu4qcgUBNdRRHehKuR6wHI1fDcQXCZctHg8VC1dcCsJuXtpI8ITuB4be6qIp0IyApkWu+ixTRgJcbUzx9XjXEjitHO2UOD2EUuFwhnuvQ8WK57eFeoFwWckIeJ7OVdo5EfaLa2WJNrO0h7zVjGNRjt7s8aVx/rXvpFf/5lN5V32cqVmnudTtbdNwPWMCBsU5x50h1fpgYFaKyhw+cF3I4eQrPS/wCWr9YaTScm+lWUps0UUNLfUb7U9SIuLx3AViVyccu6o3UuLmeTJ4gCqND+Pb6DflXbj46Xzrmm7kdEOEBTyFeBPP8ACl0jNM+AOFXXf7T6X8qHj51SQNnHxCpy43eFUywOdOF3Lkb5THH4kDJ+zh9tRb2n86Zah/8AHtL+tn/Na0iZsq0xPgqYbaG0/wCJWizzNYy7NY9FTLllUHiTSe/kMt05XioG0e6nR5SfVP8A+JrPt7TeZrTED7HmhxZtpnI5sF/DP86YlMqKD6P/AKlJ9af/ABFMqyyf0zQXXSc6Xh+rkK+FNrrkaRz/ABw86cOjOQdHN2jnRVvKN444HYaXLV8PtUmhpj+KQPGUPycHFG2k+Lp07GjxSSy+OX6r+lMLH9fT6JrGSNYjQXCRX9kXQbykiy5HBjzz9vGmep+huIzHrKqoHsbePkKQXf8AmKfx/lVep/H2/vpxfSIyDSzWGX4f0iSVc4O1SMVK9mEABSMqM43vlsGqNC+KT6SfnTzUvak+rNYP+6GujLz6gJQ0UjGIZ9SSFRhj40vSwmu7gSJLIEHtELjHjVMnxP8AEaPi/wAvX65PyrpcVFcGSez5IxWMls0skczyyHgoyMmuMxPqzI7gcDxySabN8evlSdfj1+lUL9FNUUXNojKu0KNvsjOCtBCylM/WbgWbgcmi9T+Nag7T44/QFdKXBkyUdiyMxuHATwNTS2j64CGUswHqgjGaJf2B9OqrT9fk+kfyqWxlforNNiS1ZePEg4Art/Zx9VGNjFscWHDNPLL4keZoO49h/M1m5OyqM7uhSXbcRlgEOAe2ro7yPrNzQA5XA7Aaqu/bSqbr4weQrWrJGa3EajLxYVvkpyFD3a7wZIzsXsB5Gqh+rj6VHw/qLVPTAT4mPaR5GvUyX2RXq1Ef/9k="
    }]


export const Home: React.FC = () => {
    const [openProjectId, setOpenProjectId] = useState(null);
    const [openRoomId, setOpenRoomId] = useState(null);
    const [currentBoard, setCurrentBoard] = useState(null);
    const [currentItem, setCurrentItem] = useState(null);
    const [openFolderId, setOpenFolderId] = useState(null);
    const {rooms, loading, error} = useTypeSelector(state => state.rooms)
    const {folders} = useTypeSelector(state => state.folders)
    const {projects} = useTypeSelector(state => state.projects)
    const {documents} = useTypeSelector(state => state.documents)
    const {getRoomsRequest} = useActions()
    const {getProjectsRequest} = useActions()
    const {getProjectRoomRequest} = useActions()
    const {getProjectFolderRequest} = useActions()
    const {getRoomFolderRequest} = useActions()
    const {deleteFolderRequest} = useActions()
    const {getRoomDocumentRequest} = useActions()
    const {getFolderDocumentRequest} = useActions()
    const {deleteRoomRequest} = useActions()
    const {deleteProjectRequest} = useActions()

    useMemo(() => {
        getRoomsRequest()
        console.log(rooms, 'rooms')
        getProjectsRequest()
        console.log(projects, 'projects')

    }, [])
    useEffect(() => {
        if (openProjectId) {
            getProjectRoomRequest(openProjectId)
            getProjectFolderRequest(openProjectId)
            console.log(openProjectId, 'openProjectId')
        }
    }, [openProjectId])
    useEffect(() => {
        if (openRoomId) {
            getRoomFolderRequest(openRoomId)
            getRoomDocumentRequest(openRoomId)
            console.log(openRoomId, 'openRoomId')
        }
    }, [openRoomId])
    useEffect(() => {
        if (openFolderId) {
            getFolderDocumentRequest(openFolderId)
            console.log(openFolderId, 'openFolderId')
        }
    }, [openFolderId])
    const handleDelete = async (id: any) => {
        console.log(id)
        await deleteProjectRequest(id);
        getProjectsRequest()
    };
    const deleteRoom = async (id: any) => {
        console.log(id)
        await deleteRoomRequest(id)
        if (openProjectId) {
            getProjectRoomRequest(openProjectId)
        }
    }
    const deleteFolder = async (id: any) => {
        console.log(id)
        await deleteFolderRequest(id)
        if (openProjectId) {
            getProjectFolderRequest(openProjectId)
        } else {
            if (openRoomId) {
                getRoomFolderRequest(openRoomId)
            }
        }
    }

    const toggleProject = (id: any) => {
        if (id === openProjectId) {
            console.log(id, 'idd')
            setOpenProjectId(null)
        } else {
            setOpenProjectId(id)

            console.log('gggg')
        }
    }
    const toggleRoom = (id: any) => {
        if (id === openRoomId) {
            setOpenRoomId(null)
        } else {
            setOpenRoomId(id)
        }
    }
    const toggleFolder = (id: any) => {
        if (id === openFolderId) {
            setOpenFolderId(null)
        } else {
            setOpenFolderId(id)
            console.log(id, 'folderId')
        }
    }
    //
    // function dragOverHandler(e: any) {
    //     e.preventDefault()
    //     if (e.target.className == 'item') {
    //         e.target.style.boxShadow = '0 4px 3px grey'
    //     }
    // }
    //
    // function dragLeaveHandler(e: any) {
    //     e.target.style.boxShadow = 'none'
    // }
    //
    // function dragStartHandler(e: any, item: any, m: any) {
    //     setCurrentBoard(item)
    //     setCurrentItem(m)
    // }
    //
    // function dragEndHandler(e: any) {
    //     e.target.style.boxShadow = 'none'
    // }
    //
    // function dropHandler(e: any, item: any, m: any) {
    //     console.log(m, 'jjjj')
    //     console.log(item, 'project')
    //     e.preventDefault();
    //     console.log(currentBoard, 'oooo')
    //     console.log(currentItem, 'item')
    //     const currentIndex = currentBoard.folders.indexOf(currentItem)
    //     console.log(currentIndex, 'kkkkkkk')
    // console.log(currentIndex, 'index')
    // currentBoard.folders.splice(currentIndex, 1)
    // const dropIndex = item.folders.indexOf(m)
    // console.log(dropIndex, 'kknnnn')
    // item.folders.splice(dropIndex + 1, 0, currentItem)
    // projects.map((i:any) => {
    //     if(i.id === item.id ){
    //         return item
    //     }
    //     if(i.id === currentBoard.id){
    //         return  currentBoard;
    //     }
    //     return i
    // }))
    // }
    const [board, setBoard] = useState([]);
    const [{isOver}, drop] = useDrop(()=>({
        accept: "image",
        drop:(item)=> {
            addImageToBoard(item.id)
        },
        collect:(monitor)=>({
            isOver: !!monitor.isOver()
        })
    }))

    const addImageToBoard = (id:any) =>{
        const list1 = list.filter((l)=>id === l.id)
        // setBoard([list1[0]])
        setBoard((board)=>[...board, list1[0]]);
    }
    return (
        <div>
            <div className="home">
                <div className="select-block">
                    {/*<input type="text" placeholder="Searching for ..."*/}
                    {/*/>*/}
                </div>
                <div className="test">
                    <div className="picture">  {list.map((l)=>(
                            <FolderItem key ={l.id}  url={l.url} id={l.id} />
                        )
                    )}</div>
                    <div className="board" ref={drop}>
                        {board.map((l:any)=>(
                                <FolderItem key ={l.id} url={l.url} id={l.id} />
                            )
                        )}
                    </div>
                    {/*{projects?.map((item: any) => (*/}
                    {/*    <div key={item.id}>*/}

                    {/*        {item.avatar ? (*/}
                    {/*            <img onClick={() => {*/}
                    {/*                toggleProject(item.id);*/}
                    {/*            }} className="project-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}*/}
                    {/*                 alt=""/>) : null}*/}
                    {/*        {openProjectId === item.id ? (<div>*/}
                    {/*            /!*{folders?.map((m: any) => (*!/*/}
                    {/*            /!*    <div key={m.id}*!/*/}
                    {/*            /!*        // onDragOver={(e)=>{dragOverHandler(e)}}*!/*/}
                    {/*            /!*        // onDragLeave={(e)=>{dragLeaveHandler(e)}}*!/*/}
                    {/*            /!*        // onDragStart={(e)=>{dragStartHandler(e, item, m)}}*!/*/}
                    {/*            /!*        // onDragEnd={(e)=>{dragEndHandler(e)}}*!/*/}
                    {/*            /!*        // onDrop={(e)=>{dropHandler(e, item, m)}}*!/*/}
                    {/*            /!*        // draggable={true}*!/*/}
                    {/*            /!*    >*!/*/}
                    {/*            /!*        <img className="project-img"*!/*/}
                    {/*            /!*             src={`${process.env.REACT_APP_BASE_URL}/${m.avatar}`}*!/*/}
                    {/*            /!*             alt=""/>*!/*/}
                    {/*            /!*    </div>*!/*/}
                    {/*            /!*))}*!/*/}
                    {/*          <div className="picture">  {list.map((l)=>(*/}
                    {/*                  <FolderItem key ={l.id}  url={l.url} id={l.id} />*/}
                    {/*              )*/}
                    {/*          )}</div>*/}
                    {/*            <div className="board" ref={drop}>*/}
                    {/*                {board.map((l:any)=>(*/}
                    {/*                        <FolderItem key ={l.id} url={l.url} id={l.id} />*/}
                    {/*                    )*/}
                    {/*                )}*/}
                    {/*            </div>*/}
                    {/*        </div>) : null}*/}
                    {/*    </div>*/}
                    {/*))}*/}
                </div>
                <div className="product-container item">
                    {/*<Link to="/create-project">*/}
                    {/*    <button className="btn-green">Create Room</button>*/}
                    {/*</Link>*/}
                    {/*{projects?.map((item: any) => (*/}
                    {/*    <Fragment key={item.id}>*/}
                    {/*        <div className="product item">*/}
                    {/*            {item.avatar ? (*/}
                    {/*                <img onClick={() => {*/}
                    {/*                    toggleProject(item.id);*/}
                    {/*                }} className="project-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}*/}
                    {/*                     alt=""/>) : null}*/}
                    {/*            <div className="product-name">{item.name}</div>*/}
                    {/*            <div className="btn-block">*/}
                    {/*                <Link key={item.id} to={`/project/update/${item.id}`}>*/}
                    {/*                    <button className="btn">Update</button>*/}
                    {/*                </Link>*/}
                    {/*                <button className="btn" type="button" id={item.id}*/}
                    {/*                        onClick={() => handleDelete(item.id)}>Delete*/}
                    {/*                </button>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*        <div className={openProjectId === item.id ? 'open-category' : 'close-category'}>*/}
                    {/*            {openProjectId === item.id ? (<div className="room-folder-container room">*/}
                    {/*                {*/}
                    {/*                    rooms?.map((room: any) => (*/}
                    {/*                        <div key={room.id}>*/}
                    {/*                            <div className="room-container-element">*/}
                    {/*                                <img className="room-img"*/}
                    {/*                                     onClick={() => {*/}
                    {/*                                         toggleRoom(room.id)*/}
                    {/*                                     }}*/}
                    {/*                                     src={`${process.env.REACT_APP_BASE_URL}/${room.avatar}`}*/}
                    {/*                                     alt=""/>*/}
                    {/*                                <div>{room.name}</div>*/}
                    {/*                                <div className="btn-block">*/}
                    {/*                                    <Link key={room.id} to={`/project/update/${room.id}`}>*/}
                    {/*                                        <button className="btn">Update</button>*/}
                    {/*                                    </Link>*/}
                    {/*                                    <button className="btn" type="button" id={room.id}*/}
                    {/*                                            onClick={() => deleteRoom(room.id)}>Delete*/}
                    {/*                                    </button>*/}
                    {/*                                </div>*/}
                    {/*                            </div>*/}
                    {/*                            <div*/}
                    {/*                                className={openRoomId === room.id ? 'open-category' : 'close-category'}>*/}
                    {/*                                {openRoomId === room.id ? (*/}
                    {/*                                    <div>*/}
                    {/*                                        <div> {folders.map((folder: any) => (*/}
                    {/*                                            <div key={folder.id}>*/}
                    {/*                                                <div className="room-folder-element1">*/}
                    {/*                                                    <img className="folder-img"*/}
                    {/*                                                         onClick={() => {*/}
                    {/*                                                             toggleFolder(folder.id)*/}
                    {/*                                                         }}*/}
                    {/*                                                         src={`${process.env.REACT_APP_BASE_URL}/${folder.avatar}`}*/}
                    {/*                                                         alt=""/>*/}
                    {/*                                                    {folder.name}*/}
                    {/*                                                    <div>{folder.id}</div>*/}
                    {/*                                                    <div className="btn-block">*/}
                    {/*                                                        <Link key={folder.id}*/}
                    {/*                                                              to={`/project/update/${folder.id}`}>*/}
                    {/*                                                            <button className="btn">Update*/}
                    {/*                                                            </button>*/}
                    {/*                                                        </Link>*/}
                    {/*                                                        <button className="btn" type="button"*/}
                    {/*                                                                onClick={()=>{*/}
                    {/*                                                                    deleteFolder(folder.id)*/}
                    {/*                                                                }}   id={folder.id}>Delete*/}
                    {/*                                                        </button>*/}
                    {/*                                                    </div>*/}
                    {/*                                                </div>*/}
                    {/*                                                {openFolderId === folder.id ? (*/}
                    {/*                                                    <div className="document-element-container"><div*/}
                    {/*                                                        className={openFolderId === folder.id ? 'open-folder' : 'close-folder'}>{documents.map((l: any) => (*/}
                    {/*                                                        <div className="document-element"*/}
                    {/*                                                             key={l.id}>*/}
                    {/*                                                            <img className="folder-img"*/}
                    {/*                                                                 src={`${process.env.REACT_APP_BASE_URL}/${l.avatar}`}*/}
                    {/*                                                                 alt=""/>*/}
                    {/*                                                            {l.name}*/}
                    {/*                                                            <div>{l.id}</div>*/}
                    {/*                                                            <div className="btn-block">*/}
                    {/*                                                                <Link key={l.id}*/}
                    {/*                                                                      to={`/project/update/${l.id}`}>*/}
                    {/*                                                                    <button className="btn">Update*/}
                    {/*                                                                    </button>*/}
                    {/*                                                                </Link>*/}
                    {/*                                                                <button className="btn"*/}
                    {/*                                                                        type="button"*/}
                    {/*                                                                        id={l.id}*/}
                    {/*                                                                >Delete*/}
                    {/*                                                                </button>*/}
                    {/*                                                            </div>*/}
                    {/*                                                        </div>*/}
                    {/*                                                    ))}</div></div>) : null}*/}
                    {/*                                            </div>*/}
                    {/*                                        ))}*/}
                    {/*                                        </div>*/}
                    {/*                                        <div>{documents.map((doc: any) => (*/}
                    {/*                                            <div className="room-folder-element1" key={doc.id}>*/}
                    {/*                                                <img className="folder-img"*/}
                    {/*                                                     src={`${process.env.REACT_APP_BASE_URL}/${doc.avatar}`}*/}
                    {/*                                                     alt=""/>*/}
                    {/*                                                {doc.name}*/}
                    {/*                                                <div>{doc.id}</div>*/}
                    {/*                                                <div className="btn-block">*/}
                    {/*                                                    <Link key={doc.id}*/}
                    {/*                                                          to={`/project/update/${doc.id}`}>*/}
                    {/*                                                        <button className="btn">Update</button>*/}
                    {/*                                                    </Link>*/}
                    {/*                                                    <button className="btn" type="button"*/}
                    {/*                                                            id={doc.id}*/}
                    {/*                                                    >Delete*/}
                    {/*                                                    </button>*/}
                    {/*                                                </div>*/}
                    {/*                                            </div>*/}
                    {/*                                        ))}</div>*/}
                    {/*                                    </div>*/}
                    {/*                                ) : null}*/}
                    {/*                            </div>*/}
                    {/*                        </div>*/}
                    {/*                    ))}*/}
                    {/*                {*/}
                    {/*                    folders?.map((j: any) => (*/}
                    {/*                        <div className="folder-container-element folder2" key={j.id}*/}
                    {/*                             onDragOver={(e)=>{drugOverHandler(e)}}*/}
                    {/*                             onDragLeave={(e)=>{drugLeaveHandler(e)}}*/}
                    {/*                             onDragStart={(e)=>{drugStartHandler(e, item, j)}}*/}
                    {/*                             onDragEnd={(e)=>{drugEndHandler(e)}}*/}
                    {/*                             onDrop={(e)=>{drugDropHandler(e, item, j)}}*/}
                    {/*                             draggable={true}*/}
                    {/*                        >*/}
                    {/*                            <img className="folder-img"*/}
                    {/*                                 src={`${process.env.REACT_APP_BASE_URL}/${j.avatar}`}*/}
                    {/*                                 alt=""/>*/}
                    {/*                            <div>{j.name}</div>*/}
                    {/*                            <div className="btn-block">*/}
                    {/*                                <Link key={j.id} to={`/project/update/${j.id}`}>*/}
                    {/*                                    <button className="btn">Update</button>*/}
                    {/*                                </Link>*/}
                    {/*                                <button className="btn" type="button" id={j.id}*/}
                    {/*                                >Delete*/}
                    {/*                                </button>*/}
                    {/*                            </div>*/}
                    {/*                        </div>*/}
                    {/*                    ))}*/}
                    {/*            </div>) : null}*/}
                    {/*        </div>*/}
                    {/*    </Fragment>*/}
                    {/*))}*/}
                    {/*{rooms?.map((item: any) => (*/}
                    {/*    <div className="product" key={item.id}>*/}
                    {/*        {item.avatar ? (*/}
                    {/*            <img className="room-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}*/}
                    {/*                 alt=""/>) : null}*/}
                    {/*        <div className="product-name">{item.name}</div>*/}
                    {/*        /!*<h4>{item.projects?.map((i:any)=>(*!/*/}
                    {/*        /!*    <h5 key={i.id}>{i.id}</h5>*!/*/}
                    {/*        /!*))}</h4>*!/*/}
                    {/*        <div className="btn-block">*/}
                    {/*            <Link key={item.id} to={`/room/update/${item.id}`}><button className="btn">Update</button></Link>*/}
                    {/*            <button className="btn" type="button" id={item.id} onClick={()=> handleDelete(item.id)}>Delete</button>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*))}*/}
                </div>

            </div>
        </div>
    );
}
export default Home;
