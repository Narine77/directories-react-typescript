import {Dispatch} from "redux";
import Api from "../../Api";
import {DocumentAction, DocumentActionTypes} from "../../types/document";

export const getRoomDocumentRequest = (id:number) => {
    return async (dispatch:Dispatch<DocumentAction>) => {
        try{
            dispatch({type:DocumentActionTypes.GET_ROOM_DOCUMENT_REQUEST})
            const response = await Api.getRoomDocuments(id)
                dispatch({type: DocumentActionTypes.GET_ROOM_DOCUMENTS_SUCCESS, payload: response.data})
            console.log(response, 'response')
        }
        catch (e){
            dispatch({type: DocumentActionTypes.GET_ROOM_DOCUMENTS_FAIL, payload: 'error in loading'})
        }

    }
}
// export const getFolderDocumentRequest = (id:number) => {
//     return async (dispatch:Dispatch<DocumentAction>) => {
//         try{
//             dispatch({type:DocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST})
//             const response = await Api.getFolderDocuments(id)
//             dispatch({type: DocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS, payload: response.data})
//             console.log(response, 'response')
//         }
//         catch (e){
//             dispatch({type: DocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL, payload: 'error in loading'})
//         }
//
//     }
// }
