import React, { useState } from 'react';
import _ from 'lodash';

function Input(props:any) {
  const [id] = useState(_.uniqueId('input'));
  const {
    error,
    label, iconLeft, className, iconRight, type, ...restProps
  } = props;

  return (
    <div className="input_wrapper">
      <label htmlFor={id}>{label}</label>
      {iconLeft ? <div className="input-icon input-icon-left">{iconLeft}</div> : null}
      <input
        className={`input ${className}`}
        id={id}
        type={type}
        {...restProps}
      />
      {error ? <span className="error">{error}</span> : null}
      {iconRight ? <div className="input-icon input-icon-right">{iconRight}</div> : null}
    </div>
  );
}

export default Input;
