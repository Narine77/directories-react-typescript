
import React from 'react';
import { useDrag } from 'react-dnd/dist/hooks/useDrag';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';


function FolderItem({id, url}) {
    const [{ isDragging:any}, drag] = useDrag(() => ({
        type: "image",
        item: {id: id},
        collect: (monitor) => ({
            isDragging: !!monitor.isDragging(),
        }),
    }))
    // console.log(props.avatar, 'jjjjj')
    return (
        <div className="folder-container-element item">
            {/*<img className="folder-img"*/}
            {/*     src={`${process.env.REACT_APP_BASE_URL}/${props.avatar}`}*/}
            {/*     alt=""/>*/}
            {/*<div >{props.name}</div>*/}
            {/*<div className="btn-block">*/}
            {/*    <Link key={props.id} to={`/project/update/${props.id}`}>*/}
            {/*        <button className="btn">Update</button>*/}
            {/*    </Link>*/}
            {/*    <button className="btn" type="button" id={props.id}*/}
            {/*    >Delete*/}
            {/*    </button>*/}
            {/*</div>*/}
            <img src="" ref={drag} src={url} width="150px" alt=""/>
        </div>
    );
}
FolderItem.propTypes = {
    id: PropTypes.number.isRequired,
    url: PropTypes.string.isRequired
};

export default FolderItem ;
