import React from 'react';
import {Link} from "react-router-dom";
import {useDrag} from "react-dnd";


function FolderItem(props: any) {
    const [{ isDragging:any}, drag] = useDrag(() => ({
        type: "image",
        item: {id: props.id},
        beginDrag: (p:any) =>{
          console.log(p,32234123123123)
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    }))

    return (
        <div className="room-folder-element"
        >
            <img className="folder-img project-folder"
                 ref={drag}
                 src={`${process.env.REACT_APP_BASE_URL}/${props.avatar}`}
                 alt=""/>
            {/*<div className="drop">move to</div>*/}
            <div >{props.name}</div>
            <div >{props.id}</div>
            <div className="btn-block">
                <Link key={props.id} to={`/project/update/${props.id}`}>
                    <button className="btn">Update</button>
                </Link>
                <button className="btn" type="button" id={props.id}
                >Delete
                </button>
            </div>
        </div>
    );
}

export default FolderItem ;
