export interface FormTypes {
    avatar: File,
    name: string,
    description: string
}

export interface DynamicObject {
    [key: string]: string | any
}

export interface TargetBoxProps {
    onDrop: (item: any) => void
    lastDroppedColor?: string
}
export interface DragItem {
    type: string
}
