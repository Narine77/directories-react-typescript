import {Dispatch} from "redux";
import Api from "../../Api";
import {FolderAction, FolderActionTypes} from "../../types/folder";
import {ProjectActionTypes} from "../../types/projects";
import {ProjectFolderAction, ProjectFolderActionTypes} from "../../types/projectFolder";


export const getFoldersRequest = () => {
    return async (dispatch:Dispatch<FolderAction>) => {
        try{
            dispatch({type:FolderActionTypes.GET_FOLDERS_REQUEST})
            const response = await Api.getAllRooms()
                dispatch({type: FolderActionTypes.GET_FOLDERS_SUCCESS, payload: response.data})
            console.log(response, 'response')
        }
        catch (e){
            dispatch({type: FolderActionTypes.GET_FOLDERS_FAIL, payload: 'error in loading folders'})
        }

    }
}

// export const getProjectFolderRequest = (id:number) => {
//     return async (dispatch:Dispatch<FolderAction>) => {
//         try{
//             dispatch({type:FolderActionTypes.GET_PROJECT_FOLDER_REQUEST})
//             const response = await Api.getProjectFolders(id)
//             console.log(response, 'projectfolder action' )
//             dispatch({type: FolderActionTypes.GET_PROJECT_FOLDER_SUCCESS, payload: response.data})
//         }
//         catch (e){
//             dispatch({type: FolderActionTypes.GET_PROJECT_FOLDER_FAIL, payload: 'error in loading folders'})
//         }
//     }
// }

export const getRoomFolderRequest = (id:number) => {
    return async (dispatch:Dispatch<FolderAction>) => {
        try{
            dispatch({type:FolderActionTypes.GET_ROOM_FOLDER_REQUEST})
            const response = await Api.getRoomFolders(id)
            console.log(response, 'roomfolder action');
            dispatch({type: FolderActionTypes.GET_ROOM_FOLDER_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: FolderActionTypes.GET_ROOM_FOLDER_FAIL, payload: 'error in loading folders'})
        }
    }
}

export const deleteFolderRequest = (id:number) => {
    return async (dispatch:Dispatch<FolderAction>) => {
        try{
            dispatch({type:FolderActionTypes.DELETE_FOLDER_REQUEST })
            const response = await Api.deleteRoom(id)
            dispatch({type: FolderActionTypes.DELETE_FOLDER_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: FolderActionTypes.DELETE_FOLDER_FAIL, payload: 'error in loading users'})
        }
    }
}

// export const deleteFolderProjectIdRequest = (id:number) => {
//     return async (dispatch:Dispatch<ProjectFolderAction>) => {
//         try{
//             dispatch({type:ProjectFolderActionTypes.DELETE_PROJECT_ID_REQUEST })
//             const response = await Api.deleteFolderProjectId
//             console.log(response, 'deleteFolderProjectIdRequest')
//             dispatch({type: ProjectFolderActionTypes.DELETE_PROJECT_ID_SUCCESS, payload: response.data})
//         }
//         catch (e){
//             dispatch({type: ProjectFolderActionTypes.DELETE_PROJECT_ID_FAIL, payload: 'error in loading users'})
//         }
//     }
// }

export const getSingleFolderRequest = (id:number) => {
    return async (dispatch:Dispatch<FolderAction>) => {
        try{
            dispatch({type: FolderActionTypes.GET_SINGLE_FOLDER_REQUEST})
            const response = await Api.getSingleFolder(id)
            console.log(response, 'project reducer');
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_SUCCESS, payload: response.data})
        }
        catch (e){
            dispatch({type: ProjectActionTypes.GET_SINGLE_PROJECT_FAIL, payload: 'error in loading projects'})
        }
    }
}

// export const createUserRequest = (formData:{[key:string]:string}) => {
//     return async (dispatch:Dispatch<UserAction>) => {
//         try{
//             dispatch({type:UserActionTypes.CREATE_USERS_REQUEST})
//             const request = await Api.createUsers(formData)
//                 dispatch({type: UserActionTypes.CREATE_USERS_SUCCESS, payload: {user: request.data, token: Account.getToken()}})
//
//         }
//         catch (e){
//             dispatch({type: UserActionTypes.CREATE_USERS_FAIL, payload: 'error '})
//         }
//     }
// }

