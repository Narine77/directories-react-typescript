import {projectEditTypes, projectObjectTypes} from "./projects";

export interface Room {
    name: string,
    description: string,
    avatar: string
}

export interface RoomState {
    rooms: Room[],
    loading: boolean,
    error: null | string,
    room: Room
}
export interface roomObjectTypes{
    [key:string]:string
}
export interface roomEditTypes<R>{
    room: R,
}
export interface roomCreateTypes<T>{
    room: T,
}

export enum RoomActionTypes{
    GET_PROJECT_ROOM_REQUEST = "GET_PROJECT_ROOM_REQUEST",
    GET_PROJECT_ROOM_SUCCESS = "GET_PROJECT_ROOM_SUCCESS",
    GET_PROJECT_ROOM_FAIL = "GET_PROJECT_ROOM_FAIL",

    GET_ROOMS_REQUEST = "GET_ROOMS_REQUEST",
    GET_ROOMS_SUCCESS = "GET_ROOMS_SUCCESS",
    GET_ROOMS_FAIL = "GET_ROOMS_FAIL",

    GET_ROOM_REQUEST = "GET_ROOM_REQUEST",
    GET_ROOM_SUCCESS = "GET_ROOM_SUCCESS",
    GET_ROOM_FAIL = "GET_ROOM_FAIL",

    CREATE_ROOMS_REQUEST = "CREATE_ROOMS_REQUEST",
    CREATE_ROOMS_SUCCESS = "CREATE_ROOMS_SUCCESS",
    CREATE_ROOMS_FAIL = "CREATE_ROOMS_FAIL",

    EDIT_ROOMS_REQUEST = "EDIT_ROOMS_REQUEST",
    EDIT_ROOMS_SUCCESS = "EDIT_ROOMS_SUCCESS",
    EDIT_ROOMS_FAIL = "EDIT_ROOMS_FAIL",

    DELETE_ROOMS_REQUEST = "DELETE_ROOMS_REQUEST",
    DELETE_ROOMS_SUCCESS = "DELETE_ROOMS_SUCCESS",
    DELETE_ROOMS_FAIL = "DELETE_ROOMS_FAIL",

}
// create
interface CreateRoomRequestAction{
    type: RoomActionTypes.CREATE_ROOMS_REQUEST,
    payload? : any
}

interface CreateRoomSuccessAction{
    type: RoomActionTypes.CREATE_ROOMS_SUCCESS,
    payload: roomCreateTypes<object>
}

interface CreateRoomFailAction{
    type: RoomActionTypes.CREATE_ROOMS_FAIL,
    payload? : any
}

// get
interface GetRoomsRequestAction{
    type: RoomActionTypes.GET_ROOMS_REQUEST,
    payload? : any
}

interface GetRoomsSuccessAction{
    type: RoomActionTypes.GET_ROOMS_SUCCESS,
    payload: []
}

interface GetRoomsFailAction{
    type: RoomActionTypes.GET_ROOMS_FAIL,
    payload? : any
}
//get one project room
interface GetProjectRoomRequestAction{
    type: RoomActionTypes.GET_PROJECT_ROOM_REQUEST,
    payload? : any
}

interface GetProjectRoomSuccessAction{
    type: RoomActionTypes.GET_PROJECT_ROOM_SUCCESS,
    payload: []
}

interface GetProjectRoomFailAction{
    type: RoomActionTypes.GET_PROJECT_ROOM_FAIL,
    payload? : any
}

//get one
interface GetRoomRequestAction{
    type: RoomActionTypes.GET_ROOM_REQUEST,
    payload? : any
}

interface GetRoomSuccessAction{
    type: RoomActionTypes.GET_ROOM_SUCCESS,
    payload: []
}

interface GetRoomFailAction{
    type: RoomActionTypes.GET_ROOM_FAIL,
    payload? : any
}
// edit
interface EditRoomRequestAction{
    type: RoomActionTypes.EDIT_ROOMS_REQUEST,
    payload? : any
}

interface EditRoomSuccessAction{
    type: RoomActionTypes.EDIT_ROOMS_SUCCESS,
    payload: roomEditTypes<roomObjectTypes>
}

interface EditRoomFailAction{
    type: RoomActionTypes.EDIT_ROOMS_FAIL,
    payload? : any
}


// delete
interface DeleteRoomRequestAction{
    type: RoomActionTypes.DELETE_ROOMS_REQUEST,
    payload? : any
}

interface DeleteRoomSuccessAction{
    type: RoomActionTypes.DELETE_ROOMS_SUCCESS,
    payload: []
}

interface DeleteRoomFailAction{
    type: RoomActionTypes.DELETE_ROOMS_FAIL,
    payload? : any
}


export type RoomAction = GetRoomRequestAction | GetRoomSuccessAction | GetRoomFailAction |
    GetProjectRoomRequestAction | GetProjectRoomSuccessAction | GetProjectRoomFailAction
    |  DeleteRoomRequestAction | DeleteRoomSuccessAction | DeleteRoomFailAction |
    CreateRoomRequestAction | CreateRoomSuccessAction | CreateRoomFailAction |
    EditRoomRequestAction | EditRoomSuccessAction | EditRoomFailAction |
    GetRoomsRequestAction | GetRoomsSuccessAction | GetRoomsFailAction
