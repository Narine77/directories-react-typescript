import React from 'react';
import {
  useLocation, useNavigate, useParams, useSearchParams,
} from 'react-router-dom';

// eslint-disable-next-line react/display-name
const withRouter = (Comp:any) => (props: any) => {
  const navigate = useNavigate();
  const params = useParams();
  const location = useLocation();
  const searchParams = useSearchParams();
  return <Comp {...props} navigate={navigate} params={params} location={location} searchParams={searchParams} />;
};

export default withRouter;
