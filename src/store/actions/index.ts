import * as FolderActionCreators from './folder'
import * as RoomActionCreators from './room'
import * as ProjectActionCreators from './project'
import * as DocumentActionCreators from  './document'
import * as ProjectFolderActionCreators from  './projectFolders'
import * as FolderDocumentActionCreators from  './folderDocument'
export default {
    ...FolderActionCreators,
    ...ProjectFolderActionCreators,
    ...RoomActionCreators,
    ...ProjectActionCreators,
    ...DocumentActionCreators,
    ...FolderDocumentActionCreators
}
