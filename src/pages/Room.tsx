import React, {SyntheticEvent, useEffect, useState} from 'react';
import Input from '../components/form/Input';
import _ from 'lodash';
import FileInput from '../components/form/FileInput';
import {ReactComponent as ProfileIcon} from '../assets/images/profile-image.svg';
import {toast} from 'react-toastify';
import {useActions} from '../hooks/useAction';
import {useTypeSelector} from "../hooks/userTypeSelector";
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {createRoomRequest, editRoomRequest, getSingleRoomRequest} from '../store/actions/room';
import { FormTypes} from '../helpers/interface';
import {Link, useParams} from 'react-router-dom';


export const Room: React.FC = () => {
    const [file, setFile] = useState<File | null>(null);
    const [url, setUrl] = useState<string>('')
    const {createRoomRequest} = useActions()
    const {getSingleRoomRequest} = useActions()
    const {id} = useParams();
    const {loading, room, error} = useTypeSelector(state => state.rooms)

    useEffect(() => {
        if (id) {
            if (!loading && room && !error) {
                setUrl(`${process.env.REACT_APP_BASE_URL}/${room.avatar}`)
                formik.setFieldValue('name', room.name);
                formik.setFieldValue('description', room.description);
            }
        }
    }, [loading, room])

    useEffect(() => {
        if (id) {
            console.log(id, 'uId');
            (async () => {
                await getSingleRoomRequest(+id);
            })();
        }
    }, [id])
    const formik = useFormik({
        initialValues: {
            name: '',
            description: '',
        } as FormTypes,
        validationSchema: Yup.object({
            name: Yup.string()
                .max(12, 'Must be 12 characters or less')
                .required('Required'),
            description: Yup.string()
                .max(20, 'Must be 20 characters or less')
                .required('Required')
        }),

        onSubmit: async (values: FormTypes) => {
                if (!file) {
                    return
                }
                values.avatar = file;
                console.log(values, 'vvv')
            if (!id) {
                await createRoomRequest(values);
            } else {
                if (id) {
                    await editRoomRequest(+id)
                    console.log('update')
                }
            }
        }
    });
    const uploadFile = async (image: File) => {
        setFile(image)
    }
    useEffect(() => {
        if (file) {
            const reader = new FileReader();

            reader.onloadend = function () {
                const buffer: ArrayBuffer | null | string = reader.result
                const blob = new Blob([buffer as BlobPart]);
                const blobUrl = URL.createObjectURL(blob);
                setUrl(blobUrl);
            }
            reader.readAsArrayBuffer(file)
        }
    }, [file])

    /*const handleSubmit = async (ev: SyntheticEvent)=>{
        ev.preventDefault();
        console.log(formData, 'formdatadfg')
        const data  = await createRoomRequest(formData);
        // const { payload: { data } } = await dispatch(createRoomRequest(formData));
        if (!data) {
            toast.error('Invalid data!');
        }
        console.log(formData, 'formData')
    }*/
    return (
        <div>
            <button className="btn-green"><Link to="/">Go Back</Link></button>
            <div className="create-room">
                <form onSubmit={formik.handleSubmit}>
                    <FileInput
                        label="Add Image"
                        onChange={uploadFile}
                        name="file"
                        accept="image/jpg, image/jpeg, image/png"
                    >
                        {url ? <img className="profile-image" src={url} alt=""/> : <ProfileIcon/>}

                    </FileInput>
                    <div className="input-container">
                        <Input
                            label="Name"
                            name="name"
                            type="text"
                            placeholder="Room"
                            onChange={formik.handleChange}
                            value={formik.values.name}
                        />
                        {formik.errors.name ? <p>{formik.errors.name}</p> : <></>}
                    </div>
                    <div className="input-container">
                        <Input
                            className="input-secret"
                            label="Description"
                            name="description"
                            type="text"
                            placeholder="Room"
                            onChange={formik.handleChange}
                            value={formik.values.description}
                        />
                        {formik.errors.description ? <p>{formik.errors.description}</p> : <></>}
                    </div>
                    <button type="submit" className="btn btn-save">Save</button>
                </form>
            </div>
        </div>
    );
};


// return (
//     <div>
//         <div className="create-room">
//             <form className="input-block" onSubmit={handleSubmit}>
//                 <FileInput
//                     label="Add Image"
//                     onChange={(ev: React.ChangeEvent<HTMLInputElement>):void => {
//                         try{
//                             handleChange('file', ev?.target?.files);
//                         }catch (e){
//                             console.warn(e);
//                         }
//                         //
//                         ev.target.value = '';
//                     }}
//                     accept="jpg,.jpeg,.png"
//                 >
//                     {/*<ProfileIcon/>*/}
//                     {getAvatar() ? <img src={getAvatar()} alt=""/> : <ProfileIcon/>}
//                 </FileInput>
//                 <div className="input-container">
//                     <span>Name</span>
//                     <Input
//                         className="input-secret"
//                         placeholder="Name"
//                         onChange={(ev: any) => handleChange('name', ev.target.value)}
//                         value={formData.name || ''}
//                     />
//                 </div>
//                 <div className="input-container">
//                     <span>Description</span>
//                     <Input
//                         className="input-secret"
//                         placeholder="Description"
//                         onChange={(ev: any) => handleChange('description', ev.target.value)}
//                         value={formData.description || ''}
//                     />
//                 </div>
//                 <button className="btn btn-save">Save</button>
//             </form>
//
//         </div>
//     </div>
// );
// }

export default Room;
