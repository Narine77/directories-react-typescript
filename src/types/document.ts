export interface Document {
    name: string,
    description: string,
    avatar: string
}
export interface DocumentState {
    documents: Document[],
    loading: boolean,
    error: null | string,
    document: Document
}

export enum DocumentActionTypes{
    GET_ROOM_DOCUMENT_REQUEST = "GET_ROOM_DOCUMENT_REQUEST",
    GET_ROOM_DOCUMENTS_SUCCESS = "GET_ROOM_DOCUMENTS_SUCCESS",
    GET_ROOM_DOCUMENTS_FAIL = "GET_ROOM_DOCUMENTS_FAIL",

    // GET_FOLDER_DOCUMENT_REQUEST = "GET_FOLDER_DOCUMENT_REQUEST",
    // GET_FOLDER_DOCUMENT_SUCCESS = "GET_FOLDER_DOCUMENT_SUCCESS",
    // GET_FOLDER_DOCUMENTS_FAIL = "GET_FOLDER_DOCUMENTS_FAIL",

    // GET_FOLDERS_REQUEST = "GET_FOLDERS_REQUEST",
    // GET_FOLDERS_SUCCESS = "GET_FOLDERS_SUCCESS",
    // GET_FOLDERS_FAIL = "GET_FOLDERS_FAIL",
    //
    // CREATE_FOLDERS_REQUEST = "CREATE_FOLDERS_REQUEST",
    // CREATE_FOLDERS_SUCCESS = "GET_FOLDERS_SUCCESS",
    // CREATE_FOLDERS_FAIL = "CREATE_FOLDERS_FAIL",
}
// // get
// interface GetFolderRequestAction{
//     type: FolderActionTypes.GET_FOLDERS_REQUEST,
//     payload? : any
// }
//
// interface GetFolderSuccessAction{
//     type: FolderActionTypes.GET_FOLDERS_SUCCESS,
//     payload: any
// }
//
// interface GetFolderFailAction{
//     type: FolderActionTypes.GET_FOLDERS_FAIL,
//     payload? : any
// }

// // create
// interface CreateFolderAction{
//     type: FolderActionTypes.CREATE_FOLDERS_REQUEST,
// }
//
// interface CreateFolderSuccessAction{
//     type: FolderActionTypes.CREATE_FOLDERS_SUCCESS,
//     // payload: userCreateTypes<object, string>
// }
//
// interface CreateFolderFailAction{
//     type: FolderActionTypes.CREATE_FOLDERS_FAIL,
//     payload:string
// }

// get by roomId
interface GetRoomDocumentRequestAction{
    type: DocumentActionTypes.GET_ROOM_DOCUMENT_REQUEST,
    payload? : any
}

interface GetRoomDocumentSuccessAction{
    type: DocumentActionTypes.GET_ROOM_DOCUMENTS_SUCCESS,
    payload: []
}
interface GetRoomDocumentFailAction{
    type: DocumentActionTypes.GET_ROOM_DOCUMENTS_FAIL,
    payload: []
}
// get by folderId
interface GetFolderDocumentRequestAction{
    type: DocumentActionTypes.GET_FOLDER_DOCUMENT_REQUEST,
    payload? : any
}

interface GetFolderDocumentSuccessAction{
    type: DocumentActionTypes.GET_FOLDER_DOCUMENT_SUCCESS,
    payload: []
}
interface GetFolderDocumentFailAction{
    type: DocumentActionTypes.GET_FOLDER_DOCUMENTS_FAIL,
    payload: []
}

// // get by projectId
// interface GetProjectFolderRequestAction{
//     type: FolderActionTypes.GET_PROJECT_FOLDER_REQUEST,
//     payload? : any
// }
//
// interface GetProjectFolderSuccessAction{
//     type: FolderActionTypes.GET_PROJECT_FOLDER_SUCCESS,
//     payload: []
// }
// interface GetProjectFolderFailAction{
//     type: FolderActionTypes.GET_PROJECT_FOLDER_FAIL,
//     payload: []
// }


export type DocumentAction =  GetRoomDocumentRequestAction | GetRoomDocumentSuccessAction | GetRoomDocumentFailAction
                             | GetFolderDocumentRequestAction | GetFolderDocumentSuccessAction | GetFolderDocumentFailAction
