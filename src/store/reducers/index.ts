import { combineReducers } from "redux";
import { roomReducer } from "./roomReducer";
import {projectReducer} from "./projectReducer";
import { folderReducer } from "./folderReducer";
import { documentReducer } from "./documentReducer";
import {projectFolderReducer} from "./projectFolderReducer";
import {getFolderDocumentRequest} from "../actions/folderDocument";
import {folderDocumentReducer} from "./folderDocumentReducer";

export const rootReducer = combineReducers({
    projects: projectReducer,
    rooms: roomReducer,
    folders: folderReducer,
    documents: documentReducer,
    projectfolders: projectFolderReducer,
    folderdocuments: folderDocumentReducer

})
export type RootState = ReturnType<typeof rootReducer>
