import {
    ProjectFolder,
    ProjectFolderAction,
    ProjectFolderActionTypes,
    ProjectFolderState
} from "../../types/projectFolder";


const initialState : ProjectFolderState = {
    projectfolders: [],
    loading: false,
    error: null,
    projectfolder: {}
}

export const projectFolderReducer = (state = initialState, action: ProjectFolderAction): ProjectFolderState => {
    switch (action.type) {

        case ProjectFolderActionTypes.DELETE_PROJECT_ID_REQUEST:
            return {loading: true, error: null, projectfolders: [], projectfolder: {...state.projectfolder}}
        case ProjectFolderActionTypes.DELETE_PROJECT_ID_SUCCESS:
            console.log(action.payload, 'edit folder')
            return {loading: false, error: null, projectfolders: action.payload, projectfolder: {...state.projectfolder}}
        case ProjectFolderActionTypes.DELETE_PROJECT_ID_FAIL:
            return {loading: false, error: null, projectfolders: [], projectfolder: {...state.projectfolder}}
        // edit roomId

        case ProjectFolderActionTypes.EDIT_FOLDER_ID_REQUEST:
            return {loading: true, error: null, projectfolders: [], projectfolder: {...state.projectfolder}}
        case ProjectFolderActionTypes.EDIT_FOLDER_ID_SUCCESS:
            console.log(action.payload, 'edit folder')
            return {loading: false, error: null, projectfolders: action.payload, projectfolder: {...state.projectfolder}}
        case ProjectFolderActionTypes.EDIT_FOLDER_ID_FAIL:
            return {loading: false, error: null, projectfolders: [], projectfolder: {...state.projectfolder}}
        //get one   project folders
        case ProjectFolderActionTypes.GET_PROJECT_FOLDER_REQUEST:
            return {loading: true, error: null, projectfolders: [], projectfolder: action.payload as ProjectFolder}
        case ProjectFolderActionTypes.GET_PROJECT_FOLDER_SUCCESS:
            console.log(action.payload, 'projecfolders reducer')
            return {loading: false, error: null, projectfolders: action.payload, projectfolder: {...action.payload}}
        case ProjectFolderActionTypes.GET_PROJECT_FOLDER_FAIL:
            return {loading: false, error: null, projectfolders: [], projectfolder: action.payload as ProjectFolder}

        default: {
            return state
        }
    }
}
