import React, {Fragment, useEffect, useMemo, useState} from 'react';
import {useTypeSelector} from '../hooks/userTypeSelector';
import {useActions} from "../hooks/useAction";
import {getProjectsRequest} from "../store/actions/project";
import {Link} from 'react-router-dom';
import Button from '../components/form/Button';
import {useDispatch} from 'react-redux';
import {deleteRoomRequest, getProjectRoomRequest} from "../store/actions/room";
import Room from "./Room";
import {
    deleteFolderRequest,
    getSingleFolderRequest
} from "../store/actions/folder";
import {getRoomDocumentRequest} from "../store/actions/document";
import FolderItem from '../components/FolderItem';
import DocumentItem from "../components/DocumentItem";
import {useDrop} from "react-dnd";
import {editFolderRoomIdRequest} from "../store/actions/projectFolders";

export const Home: React.FC = (props :any) => {

    const [openProjectId, setOpenProjectId] = useState(null);
    const [openRoomId, setOpenRoomId] = useState(null);
    const [itemId, setItemId] = useState(null);
    const [openFolderId, setOpenFolderId] = useState(null);
    const {rooms, loading, error} = useTypeSelector(state => state.rooms)
    const {folders} = useTypeSelector(state => state.folders)
    const {projectfolders} = useTypeSelector(state => state.projectfolders)
    const {projects} = useTypeSelector(state => state.projects)
    const {folderdocuments} = useTypeSelector(state => state.folderdocuments)
    const {documents} = useTypeSelector(state => state.documents)
    const {getProjectsRequest} = useActions()
    const {getProjectRoomRequest} = useActions()
    const {getProjectFolderRequest} = useActions()
    const {getRoomFolderRequest} = useActions()
    const {deleteFolderRequest} = useActions()
    const {deleteFolderProjectIdRequest} = useActions()
    const {getSingleFolderRequest} = useActions()
    const {editFolderRoomIdRequest} = useActions()
    const {getRoomDocumentRequest} = useActions()
    const {getFolderDocumentRequest} = useActions()
    const {deleteRoomRequest} = useActions()
    const {deleteProjectRequest} = useActions()

    useMemo(() => {
        // getRoomsRequest()
        getProjectsRequest()

    }, [])

    useMemo(() => {
        if (openProjectId) {
            getProjectRoomRequest(openProjectId)
            getProjectFolderRequest(openProjectId)
            console.log(openProjectId, 'openProjectId')
        }
    }, [openProjectId])
    useEffect(() => {
        if (openRoomId) {
            getRoomFolderRequest(openRoomId)
            getRoomDocumentRequest(openRoomId)
            console.log(openRoomId, 'openRoomId')
        }
    }, [openRoomId])
    // useEffect(() => {
    //     if (openFolderId) {
    //         getFolderDocumentRequest(openFolderId)
    //         console.log(openFolderId, 'openFolderId')
    //     }
    // }, [openFolderId])

    const handleDelete = async (id: any) => {
        console.log(id)
        await deleteProjectRequest(id);
        getProjectsRequest()
    };
    const deleteRoom = async (id: any) => {
        console.log(id)
        await deleteRoomRequest(id)
        if (openProjectId) {
            getProjectRoomRequest(openProjectId)
        }
    }
    const deleteFolder = async (id: any) => {
        console.log(id)
        await deleteFolderRequest(id)
        if (openProjectId) {
            getProjectFolderRequest(openProjectId)
        }else{
            if (openRoomId) {
                getRoomFolderRequest(openRoomId)
            }
        }
    }

    const toggleProject = (id: any) => {
        getProjectRoomRequest(id)
        getProjectFolderRequest(id)
        if (id === openProjectId) {
            console.log(id, 'idd')
            setOpenProjectId(null)
        } else {
            setOpenProjectId(id)
            getProjectRoomRequest(id)
            getProjectFolderRequest(id)

        }
    }
    const toggleRoom = (id: any) => {
        getRoomFolderRequest(id)
        getRoomDocumentRequest(id)
        if (id === openRoomId) {
            setOpenRoomId(null)
        } else {
            setOpenRoomId(id)
        }
    }
    const toggleFolder = (id: any) => {
        getFolderDocumentRequest(id)
        if (id === openFolderId) {
            setOpenFolderId(null)
        } else {
            setOpenFolderId(id)
            console.log(id, 'folderId')
        }
    }

    const [board, setBoard] = useState(null);
    const [{isOver}, drop] = useDrop(()=>({
        accept: "image",
        drop:(item,test)=> addImageToBoard(item.id, test ),
        collect:(monitor)=>({
            isOver: !!monitor.isOver(),
            canDrop: !!monitor.canDrop(),
            didDrop: !!monitor.didDrop(),
        })
    }))

    const addImageToBoard = (id:any, monitor: any) => {
        console.log(id, 'deleteProjectId', monitor.getDropResult())
        console.log(id, 'deleteProjectId', monitor.getDropResult())
        deleteFolderProjectIdRequest(id)
    }

    // const  handleEvent = (id:any) => {
    //     editFolderRoomIdRequest(id)
    //     if (id === itemId) {
    //         setItemId(null)
    //     } else {
    //         console.log(id, 'tttttt')
    //         setItemId(id)
    //     }
    // }
    useEffect(()=>{
        if(itemId){
            console.log(itemId, 'editFolderRoomIdRequest')
            editFolderRoomIdRequest(itemId)
        }
    }, [itemId])
    return (
        <div>
            <div className="home">
                <div className="select-block">
                    {/*<input type="text" placeholder="Searching for ..."*/}
                    {/*/>*/}
                </div>
                <div className="product-container">
                    <Link to="/create-project">
                        <button className="btn-green">Create Project</button>
                    </Link>
                    {projects?.map((item: any) => (
                        <Fragment key={item.id}>
                            <div className="product">
                                {item.avatar ? (
                                    <img onClick={() => {
                                        toggleProject(item.id);
                                    }} className="project-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}
                                         alt=""/>) : null}

                                <div className="product-name">{item.name}</div>
                                <div className="btn-block">
                                    <Link key={item.id} to={`/project/edit/${item.id}`}>
                                        <button className="btn">Update</button>
                                    </Link>
                                    <button className="btn" type="button" id={item.id}
                                            onClick={() => handleDelete(item.id)}>Delete
                                    </button>
                                </div>
                            </div>
                            <div className={openProjectId === item.id ? 'open-category' : 'close-category'}>
                                {openProjectId === item.id ? (<div className="room-folder-container">
                                    {
                                        rooms?.map((room: any) => (
                                            <div key={room.id}
                                                 ref={drop}>
                                                <div className="room-container-element">
                                                    <img className="room-img"
                                                         onClick={() => {
                                                             toggleRoom(room.id)
                                                         }}
                                                         onMouseOver={()=>{handleEvent(room.id)} }
                                                         src={`${process.env.REACT_APP_BASE_URL}/${room.avatar}`}
                                                         alt=""/>
                                                    <div>{room.name}</div>
                                                    <div>{room.id}</div>
                                                    <div className="btn-block">
                                                        <Link key={room.id} to={`/room/update/${room.id}`}>
                                                            <button className="btn">Update</button>
                                                        </Link>
                                                        <button className="btn" type="button" id={room.id}
                                                                onClick={() => deleteRoom(room.id)}>Delete
                                                        </button>
                                                    </div>
                                                </div>
                                                <div
                                                    className={openRoomId === room.id ? 'open-category' : 'close-category'}>
                                                    {openRoomId === room.id ? (
                                                        <div>
                                                            <div> {folders.map((folder: any) => (
                                                                <div key={folder.id}>
                                                                    <div className="room-folder-element1">
                                                                        <img className="folder-img"
                                                                             onClick={() => {
                                                                                 toggleFolder(folder.id)
                                                                             }}
                                                                             src={`${process.env.REACT_APP_BASE_URL}/${folder.avatar}`}
                                                                             alt=""/>
                                                                        {folder.name}
                                                                        <div>{folder.id}</div>
                                                                        <div className="btn-block">
                                                                            <Link key={folder.id}
                                                                                  to={`/project/update/${folder.id}`}>
                                                                                <button className="btn">Update
                                                                                </button>
                                                                            </Link>
                                                                            <button className="btn" type="button"
                                                                                    onClick={()=>{
                                                                                        deleteFolder(folder.id)
                                                                                    }}   id={folder.id}>Delete
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    {openFolderId === folder.id ? (
                                                                        <div className="document-element-container"><div
                                                                            className={openFolderId === folder.id ? 'open-folder' : 'close-folder'}>
                                                                            {folderdocuments.map((l: any) => (
                                                                                <div className="document-element"
                                                                                     key={l.id}>
                                                                                    <img className="folder-img"
                                                                                         src={`${process.env.REACT_APP_BASE_URL}/${l.avatar}`}
                                                                                         alt=""/>
                                                                                    {l.name}
                                                                                    <div>{l.id}</div>
                                                                                    <div className="btn-block">
                                                                                        <Link key={l.id}
                                                                                              to={`/project/update/${l.id}`}>
                                                                                            <button className="btn">Update
                                                                                            </button>
                                                                                        </Link>
                                                                                        <button className="btn"
                                                                                                type="button"
                                                                                                id={l.id}
                                                                                        >Delete
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            ))}</div></div>) : null}
                                                                </div>
                                                            ))}
                                                            </div>
                                                            <div>{documents.map((doc: any) => (
                                                                <DocumentItem key={doc.id} {...doc} />
                                                            ))}</div>
                                                        </div>
                                                    ) : null}
                                                </div>
                                            </div>
                                        ))}
                                    {/*global folders*/}
                                    {
                                        projectfolders?.map((j: any) => (
                                            <FolderItem  key={j.id}  {...j}  />
                                            // <div className="folder-container-element item" key={j.id}
                                            // >
                                            //     <img className="folder-img"
                                            //          src={`${process.env.REACT_APP_BASE_URL}/${j.avatar}`}
                                            //          alt=""/>
                                            //     <div>{j.name}</div>
                                            //     <div className="btn-block">
                                            //         <Link key={j.id} to={`/project/update/${j.id}`}>
                                            //             <button className="btn">Update</button>
                                            //         </Link>
                                            //         <button className="btn" type="button" id={j.id}
                                            //         >Delete
                                            //         </button>
                                            //     </div>
                                            // </div>
                                        ))}
                                </div>) : null}
                            </div>
                        </Fragment>
                    ))}
                    {/*<div className="board" ref={drop}></div>*/}
                    {/*{rooms?.map((item: any) => (*/}
                    {/*    <div className="product" key={item.id}>*/}
                    {/*        {item.avatar ? (*/}
                    {/*            <img className="room-img" src={`${process.env.REACT_APP_BASE_URL}/${item.avatar}`}*/}
                    {/*                 alt=""/>) : null}*/}
                    {/*        <div className="product-name">{item.name}</div>*/}
                    {/*        /!*<h4>{item.projects?.map((i:any)=>(*!/*/}
                    {/*        /!*    <h5 key={i.id}>{i.id}</h5>*!/*/}
                    {/*        /!*))}</h4>*!/*/}
                    {/*        <div className="btn-block">*/}
                    {/*            <Link key={item.id} to={`/room/update/${item.id}`}><button className="btn">Update</button></Link>*/}
                    {/*            <button className="btn" type="button" id={item.id} onClick={()=> handleDelete(item.id)}>Delete</button>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*))}*/}
                </div>

            </div>
        </div>
    );
}
export default Home;
