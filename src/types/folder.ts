
export interface Folder {
    name: string,
    description: string,
    avatar: string
}

export interface FolderState {
    folders: Folder[],
    loading: boolean,
    error: null | string,
    folder: Folder
}

export enum FolderActionTypes {
    GET_ROOM_FOLDER_REQUEST = "GET_ROOM_FOLDER_REQUEST",
    GET_ROOM_FOLDER_SUCCESS = "GET_ROOM_FOLDER_SUCCESS",
    GET_ROOM_FOLDER_FAIL = "GET_ROOM_FOLDER_FAIL",

    GET_SINGLE_FOLDER_REQUEST = "GET_SINGLE_FOLDER_REQUEST",
    GET_SINGLE_FOLDER_SUCCESS = "GET_SINGLE_FOLDER_SUCCESS",
    GET_SINGLE_FOLDER_FAIL = "GET_SINGLE_FOLDER_FAIL",

    GET_PROJECT_FOLDER_REQUEST = "GET_PROJECT_FOLDER_REQUEST",
    GET_PROJECT_FOLDER_SUCCESS = "GET_PROJECT_FOLDER_SUCCESS",
    GET_PROJECT_FOLDER_FAIL = "GET_PROJECT_FOLDER_FAIL",

    GET_FOLDERS_REQUEST = "GET_FOLDERS_REQUEST",
    GET_FOLDERS_SUCCESS = "GET_FOLDERS_SUCCESS",
    GET_FOLDERS_FAIL = "GET_FOLDERS_FAIL",

    DELETE_FOLDER_REQUEST = "DELETE_FOLDER_REQUEST",
    DELETE_FOLDER_SUCCESS = "DELETE_FOLDER_SUCCESS",
    DELETE_FOLDER_FAIL = "DELETE_FOLDER_FAIL",

    DELETE_PROJECT_ID_REQUEST = "DELETE_PROJECT_ID_REQUEST",
    DELETE_PROJECT_ID_SUCCESS = "DELETE_PROJECT_ID_SUCCESS",
    DELETE_PROJECT_ID_FAIL = "DELETE_PROJECT_ID_FAIL",
}

// get
interface GetFolderRequestAction {
    type: FolderActionTypes.GET_FOLDERS_REQUEST,
    payload?: any
}

interface GetFolderSuccessAction {
    type: FolderActionTypes.GET_FOLDERS_SUCCESS,
    payload: any
}

interface GetFolderFailAction {
    type: FolderActionTypes.GET_FOLDERS_FAIL,
    payload?: any
}

// // create
// interface CreateFolderAction{
//     type: FolderActionTypes.CREATE_FOLDERS_REQUEST,
// }
//
// interface CreateFolderSuccessAction{
//     type: FolderActionTypes.CREATE_FOLDERS_SUCCESS,
//     // payload: userCreateTypes<object, string>
// }
//
// interface CreateFolderFailAction{
//     type: FolderActionTypes.CREATE_FOLDERS_FAIL,
//     payload:string
// }

// get by roomId
interface GetRoomFolderRequestAction {
    type: FolderActionTypes.GET_ROOM_FOLDER_REQUEST,
    payload?: any
}

interface GetRoomFolderSuccessAction {
    type: FolderActionTypes.GET_ROOM_FOLDER_SUCCESS,
    payload: []
}

interface GetRoomFolderFailAction {
    type: FolderActionTypes.GET_ROOM_FOLDER_FAIL,
    payload: []
}

// get single folder
interface GetSingleFolderRequestAction {
    type: FolderActionTypes.GET_SINGLE_FOLDER_REQUEST,
    payload?: any
}

interface GetSingleFolderSuccessAction {
    type: FolderActionTypes.GET_SINGLE_FOLDER_SUCCESS,
    payload: []
}

interface GetSingleFolderFailAction {
    type: FolderActionTypes.GET_SINGLE_FOLDER_FAIL,
    payload: []
}

// // get by projectId
// interface GetProjectFolderRequestAction {
//     type: FolderActionTypes.GET_PROJECT_FOLDER_REQUEST,
//     payload?: any
// }
//
// interface GetProjectFolderSuccessAction {
//     type: FolderActionTypes.GET_PROJECT_FOLDER_SUCCESS,
//     payload: []
// }
//
// interface GetProjectFolderFailAction {
//     type: FolderActionTypes.GET_PROJECT_FOLDER_FAIL,
//     payload: []
// }

// delete
interface DeleteFolderRequestAction {
    type: FolderActionTypes.DELETE_FOLDER_SUCCESS,
    payload?: any
}

interface DeleteFolderSuccessAction {
    type: FolderActionTypes.DELETE_FOLDER_REQUEST,
    payload: []
}

interface DeleteFolderFailAction {
    type: FolderActionTypes.DELETE_FOLDER_FAIL,
    payload?: any
}

// delete project id
interface DeleteProjectIdRequestAction {
    type: FolderActionTypes.DELETE_PROJECT_ID_REQUEST,
    payload?: any
}

interface DeleteProjectIdSuccessAction {
    type: FolderActionTypes.DELETE_PROJECT_ID_SUCCESS,
    payload: []
}

interface DeleteProjectIdFailAction {
    type: FolderActionTypes.DELETE_PROJECT_ID_FAIL,
    payload?: any
}


export type FolderAction = GetFolderRequestAction | GetFolderSuccessAction | GetFolderFailAction
    // | GetProjectFolderRequestAction | GetProjectFolderSuccessAction | GetProjectFolderFailAction
    | GetRoomFolderRequestAction | GetRoomFolderSuccessAction | GetRoomFolderFailAction
    | DeleteFolderRequestAction | DeleteFolderSuccessAction | DeleteFolderFailAction |
      GetSingleFolderRequestAction | GetSingleFolderSuccessAction | GetSingleFolderFailAction
    | DeleteProjectIdRequestAction | DeleteProjectIdSuccessAction | DeleteProjectIdFailAction
// |  CreateFolderAction | CreateFolderSuccessAction | CreateFolderFailAction

