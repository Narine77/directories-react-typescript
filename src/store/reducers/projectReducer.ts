import {ProjectAction, ProjectActionTypes, ProjectState} from "../../types/projects";
import {Room} from "../../types/room";

const initialState : ProjectState = {
    projects: [],
    loading: false,
    error: null,
    project: {}
}

export const projectReducer = (state = initialState, action: ProjectAction): ProjectState => {
    switch (action.type) {
        // create
        case ProjectActionTypes.CREATE_PROJECT_REQUEST:
            return {loading: true, error: null, projects: [], project: {...state.project}}
        case ProjectActionTypes.CREATE_PROJECT_SUCCESS:
            return {loading: true, error: null, projects: action.payload, project: {...state.project}}
        case ProjectActionTypes.CREATE_PROJECT_FAIL:
            return {loading: true, error: null, projects: [], project:{...state.project}}
// get
        case ProjectActionTypes.GET_PROJECTS_REQUEST:
            return {loading:true, error: null, projects: [], project: {...state.project}}
        case ProjectActionTypes.GET_PROJECTS_SUCCESS:
            console.log(action.payload, 'project')
            return {loading:false, error: null, projects: action.payload, project: {...state.project}}
        case ProjectActionTypes.GET_PROJECTS_FAIL:
            return {loading:false, error: null, projects: [], project:{...state.project}}
// get all
        case ProjectActionTypes.GET_All_PROJECTS_REQUEST:
            return {loading:true, error: null, projects: [], project: {...state.project}}
        case ProjectActionTypes.GET_All_PROJECTS_SUCCESS:
            return {loading:false, error: null, projects: action.payload, project: {...state.project}}
        case ProjectActionTypes.GET_ALL_PROJECTS_FAIL:
            return {loading:false, error: null, projects: [], project: {...state.project}}

        // get one project
        case ProjectActionTypes.GET_SINGLE_PROJECT_REQUEST:
            return {loading: true, error: null, projects: [], project: action.payload as Room}
        case ProjectActionTypes.GET_SINGLE_PROJECT_SUCCESS:
            console.log(action.payload, 'project SINGLE_PROJECT_SUCCESS')
            return {loading: false, error: null, projects: action.payload, project: {...action.payload}}
        case ProjectActionTypes.GET_SINGLE_PROJECT_FAIL:
            return {loading: false, error: null, projects: [], project: action.payload as Room}

        // edit project
        case ProjectActionTypes.EDIT_PROJECT_REQUEST:
            return {loading: true, error: null, projects: [], project: {...state.project}}
        case ProjectActionTypes.EDIT_PROJECT_SUCCESS:
            console.log(action.payload, 'edit project')
            return {loading: false, error: null, projects: action.payload, project: {...state.project}}
        case ProjectActionTypes.EDIT_PROJECT_FAIL:
            return {loading: false, error: null, projects: [], project: {...state.project}}

        default: {
            return state
        }
    }
}
